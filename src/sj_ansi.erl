%%% @doc
%%% A small library for ANSI escape codes.
%%% @end

-module(sj_ansi).
-vsn("0.1.0").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Colors
-export([black/0,   dk_gray/0,
         red/0,     lt_red/0,
         green/0,   lt_green/0,
         brown/0,   yellow/0,
         blue/0,    lt_blue/0,
         purple/0,  lt_purple/0,
         cyan/0,    lt_cyan/0,
         gray/0,    white/0]).
%% Screens
-export([greet_screen/0, color_check/0]).


%%% Colors

black()     -> [27,91,48,48,59,51,48,109].
dk_gray()   -> [27,91,48,49,59,51,48,109].
red()       -> [27,91,48,48,59,51,49,109].
lt_red()    -> [27,91,48,49,59,51,49,109].
green()     -> [27,91,48,48,59,51,50,109].
lt_green()  -> [27,91,48,49,59,51,50,109].
brown()     -> [27,91,48,48,59,51,51,109].
yellow()    -> [27,91,48,49,59,51,51,109].
blue()      -> [27,91,48,48,59,51,52,109].
lt_blue()   -> [27,91,48,48,59,51,52,109].
purple()    -> [27,91,48,48,59,51,53,109].
lt_purple() -> [27,91,48,49,59,51,53,109].
cyan()      -> [27,91,48,48,59,51,54,109].
lt_cyan()   -> [27,91,48,49,59,51,54,109].
gray()      -> [27,91,48,48,59,51,55,109].
white()     -> [27,91,48,49,59,51,55,109].



%%% Screens

greet_screen() ->
  [[white(),
    " ╔═════════════════════════════════════════════════════════════════════════════╗"],
   " ║                                                                             ║",
   [" ║                            ",
    cyan(), "SPACE JERKS!",
    white(), "                                     ║"],
   " ║                                                                             ║",
   [" ║                                          ",
    gray(), "▃▆▁", yellow(), "   ▁                            ", white(), "║"],
   [" ║                             ",
    gray(), "▁▂▃▄▅▆▇▅▅▅▆▇█████", yellow(), "═▔┅                            ",
    white(), "║"],
   [" ║                               ",
    gray(), "▔▔▀     ▔▀▀▀▀   ", yellow(), "▔▀                            ",
    white(), "║"],
   " ║                                                                             ║",
   " ║                                                                             ║",
   [" ╚═════════════════════════════════════════════════════════════════════════════╝",
    gray()]].


color_check() ->
    [gray(), "Color check time!",
     ["Black        : ", black(),     "BLACK",        gray()],
     ["Dark Gray    : ", dk_gray(),   "DARK GRAY",    gray()],
     ["Red          : ", red(),       "RED",          gray()],
     ["Light Red    : ", lt_red(),    "LIGHT RED",    gray()],
     ["Green        : ", green(),     "GREEN",        gray()],
     ["Light Green  : ", lt_green(),  "LIGHT GREEN",  gray()],
     ["Brown        : ", brown(),     "BROWN",        gray()],
     ["Yellow       : ", yellow(),    "YELLOW",       gray()],
     ["Blue         : ", blue(),      "BLUE",         gray()],
     ["Light Blue   : ", lt_blue(),   "LIGHT BLUE",   gray()],
     ["Purple       : ", purple(),    "PURPLE",       gray()],
     ["Light Purple : ", lt_purple(), "LIGHT PURPLE", gray()],
     ["Cyan         : ", cyan(),      "CYAN",         gray()],
     ["Light Cyan   : ", lt_cyan(),   "LIGHT CYAN",   gray()],
     ["Gray         : ", gray(),      "GRAY",         gray()],
     ["White        : ", white(),     "WHITE",        gray()]].
