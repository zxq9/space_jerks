%%% @doc
%%% Space Jerks Ship Manager
%%% @end

-module(sj_ship_man).
-vsn("0.1.0").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Service Interface
-export([lookup/1, lookup_if_mine/2, christen/3, update/2, launch/2, cache/0, wipe/0]).
%% gen_server
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {serial = 1  :: space_jerks:ship_id(),
         ships  = [] :: [ship()]}).

-record(ship,
        {id   = 1    :: space_jerks:ship_id(),
         pid  = none :: none | pid(),
         mon  = none :: none | reference(),
         info = none :: none | sj_ship:info()}).

-type state() :: #s{}.
-type ship()  :: #ship{}.



%%% Service Interface

-spec lookup(ID) -> {ok, PID} | error
    when ID  :: space_jerks:ship_id(),
         PID :: pid().

lookup(ID) ->
    gen_server:call(?MODULE, {lookup, ID}).


-spec lookup_if_mine(ID, Owner) -> {ok, pid()} | error
    when ID    :: space_jerks:ship_id(),
         Owner :: space_jerks:owner().

lookup_if_mine(ID, Owner) ->
    gen_server:call(?MODULE, {lookup_if_mine, ID, Owner}).


-spec christen(Type, Make, Name) -> {ok, ID, PID}
    when Type :: space_jerks:ship_type(),
         Make :: space_jerks:ship_make(),
         Name :: space_jerks:ship_name(),
         ID   :: space_jerks:ship_id(),
         PID  :: pid().

christen(Type, Make, Name) ->
    gen_server:call(?MODULE, {christen, Type, Make, Name}).


-spec update(ID, Info) -> ok
    when ID   :: space_jerks:ship_id(),
         Info :: sj_ship:info().

update(ID, Info) ->
    gen_server:cast(?MODULE, {update, ID, Info}).


-spec launch(ID, Info) -> ok
    when ID   :: space_jerks:ship_id(),
         Info :: sj_ship:info().

launch(ID, Info) ->
    gen_server:cast(?MODULE, {launch, ID, Info}).


-spec cache() -> ok.

cache() ->
    gen_server:cast(?MODULE, cache).


-spec wipe() -> ok.

wipe() ->
    gen_server:call(?MODULE, wipe).


%%% Startup Functions


-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason :: term()}.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {ok, state()}.

init(none) ->
    Path = cache_path(),
    State =
        case file:read_file(Path) of
            {ok, Bin}       -> populate(Bin, #s{});
            {error, enoent} -> #s{}
        end,
    {ok, State}.


populate(Bin, State) ->
    {1, Serial, ShipInfo} = binary_to_term(Bin),
    Manifest =
        fun(Info, S = #s{ships = Ships}) ->
            {ok, PID} = supervisor:start_child(sj_ship_sup, [Info]),
            Mon = monitor(process, PID),
            ID = element(2, Info),
            Ship = #ship{id = ID, pid = PID, mon = Mon, info = Info},
            NewShips = lists:keystore(ID, #ship.id, Ships, Ship),
            S#s{ships = NewShips}
        end,
    NewState = lists:foldl(Manifest, State, ShipInfo),
    NewState#s{serial = Serial}.



%%% gen_server Message Handling Callbacks


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().

handle_call({lookup, ID}, _, State) ->
    Response = do_lookup(ID, State),
    {reply, Response, State};
handle_call({lookup_if_mine, ID, Owner}, _, State) ->
    Response = do_lookup_if_mine(ID, Owner, State),
    {reply, Response, State};
handle_call({christen, Type, Make, Name}, _, State) ->
    {Response, NewState} = do_christen(Type, Make, Name, State),
    {reply, Response, NewState};
handle_call(wipe, _, State) ->
    NewState = do_wipe(State),
    {reply, ok, NewState};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "~p Unexpected call from ~tp: ~tp~n", [self(), From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_cast({update, ID, Info}, State) ->
    NewState = do_update(ID, Info, State),
    {noreply, NewState};
handle_cast(cache, State) ->
    ok = do_cache(State),
    {noreply, State};
handle_cast({launch, ID, Info}, State) ->
    NewState = do_launch(ID, Info, State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "~p Unexpected cast: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_info({'DOWN', Mon, process, Pid, Reason}, State) ->
    NewState = handle_down(Mon, Pid, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "~p Unexpected info: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


handle_down(_, _, _, State) ->
    State.



%%% OTP Service Functions

-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().

code_change(_, State, _) ->
    {ok, State}.


-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().

terminate(_, _) ->
    ok.



%%% Doer Functions

do_lookup(ID, #s{ships = Ships}) ->
    case lists:keyfind(ID, #ship.id, Ships) of
        #ship{pid = PID} -> {ok, PID};
        false            -> error
    end.


do_lookup_if_mine(ID, Owner, #s{ships = Ships}) ->
    case lists:keyfind(ID, #ship.id, Ships) of
        #ship{pid = PID} ->
            case sj_ship:owner(PID) == Owner of
                true  -> {ok, PID};
                false -> error
            end;
        false ->
            error
    end.


do_christen(Type, Make, Name, State = #s{serial = ID, ships = Ships}) ->
    {ok, PID} = supervisor:start_child(sj_ship_sup, [ID, Type, Make, Name]),
    Mon = monitor(process, PID),
    NewID = ID + 1,
    Ship = #ship{id = ID, pid = PID, mon = Mon},
    NewShips = lists:keystore(ID, #ship.id, Ships, Ship),
    NewState = State#s{serial = NewID, ships = NewShips},
    {{ok, ID, PID}, NewState}.


do_launch(ID, Info, State = #s{ships = Ships}) ->
    NewShips =
        case lists:keyfind(ID, #ship.id, Ships) of
            Selected = #ship{} ->
                Ship = Selected#ship{info = Info},
                lists:keystore(ID, #ship.id, Ships, Ship);
            false ->
                Ships
        end,
    State#s{ships = NewShips}.


do_update(ID, Info, State = #s{ships = Ships}) ->
    NewShips =
        case lists:keyfind(ID, #ship.id, Ships) of
            Selected = #ship{} ->
                Ship = Selected#ship{info = Info},
                lists:keystore(ID, #ship.id, Ships, Ship);
            false ->
                Ships
        end,
    State#s{ships = NewShips}.


do_cache(#s{serial = Serial, ships = Ships}) ->
    ShipInfo = lists:map(fun ship_update/1, Ships),
    Version = 1,
    Path = cache_path(),
    Data = term_to_binary({Version, Serial, ShipInfo}, [{compressed, 9}]),
    file:write_file(Path, Data).

ship_update(#ship{pid = PID}) ->
    sj_ship:status(PID).


do_wipe(#s{ships = Ships}) ->
    Kill =
        fun(#ship{pid = PID, mon = Mon}) ->
            true = demonitor(Mon),
            supervisor:terminate_child(sj_ship_sup, PID)
        end,
    ok = lists:foreach(Kill, Ships),
    Path = cache_path(),
    ok =
        case filelib:is_file(Path) of
            true  -> zx_lib:rm(Path);
            false -> ok
        end,
    #s{}.


cache_path() ->
    filename:join(zx_lib:path(var, "otpr", "space_jerks"), "ships.erlbin").
