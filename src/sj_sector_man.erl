%%% @doc
%%% Space Jerks Sector Manager
%%% @end

-module(sj_sector_man).
-vsn("0.1.0").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Service Interface
-export([lookup/1, sector_count/0]).
%% Immortal Interface
-export([bigbang/1, bigcrunch/0]).
%% gen_server
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {sectors = none :: none | #{space_jerks:sector() := sector_info()},
         mons    = #{}  :: #{reference() := space_jerks:sector()}}).


-type state()       :: #s{}.
-type sector_info() :: {PID :: pid(), Warps :: [space_jerks:sector()]}.



%%% Service Interface

-spec lookup(ID) -> {ok, pid()} | error
    when ID :: space_jerks:sector().

lookup(ID) ->
    gen_server:call(?MODULE, {lookup, ID}).


-spec sector_count() -> none | pos_integer().

sector_count() ->
    gen_server:call(?MODULE, sector_count).


%%% Immortal Interface


-spec bigbang(Magnitude) -> ok | error
    when Magnitude :: pos_integer().

bigbang(Magnitude) ->
    bigbang(Magnitude, 6).


-spec bigbang(Magnitude, MaxWarps) -> ok | error
    when Magnitude :: pos_integer(),
         MaxWarps  :: 2..10.

bigbang(Magnitude, MaxWarps) ->
    gen_server:call(?MODULE, {bigbang, Magnitude, MaxWarps}, infinity).


-spec bigcrunch() -> ok.

bigcrunch() ->
    gen_server:call(?MODULE, bigcrunch).



%%% gen_server

-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason :: term()}.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {ok, state()}.

init(none) ->
    Path = cache_path(),
    State =
        case file:read_file(Path) of
            {ok, Bin} ->
                {1, Topograph} = binary_to_term(Bin),
                Magnitude = maps:size(Topograph),
                {Sectors, Mons} = manifest(Topograph, #{}, 1, Magnitude),
                ok = peer(Sectors),
                #s{sectors = Sectors, mons = Mons};
            {error, enoent} ->
                #s{}
        end,
    {ok, State}.


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().

handle_call({lookup, ID}, _, State) ->
    Response = do_lookup(ID, State),
    {reply, Response, State};
handle_call(sector_count, _, State) ->
    Response = do_sector_count(State),
    {reply, Response, State};
handle_call({bigbang, Magnitude, MaxWarps}, _, State) ->
    {Response, NewState} = do_bigbang(Magnitude, MaxWarps, State),
    {reply, Response, NewState};
handle_call(bigcrunch, _, State) ->
    NewState = do_bigcrunch(State),
    {reply, ok, NewState};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "~p Unexpected call from ~tp: ~tp~n", [self(), From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_cast(Unexpected, State) ->
    ok = log(warning, "~p Unexpected cast: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_info({'DOWN', Mon, process, PID, Reason}, State) ->
    NewState = handle_down(Mon, PID, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_down(Mon, PID, Reason, State = #s{sectors = Sectors, mons = Mons}) ->
    case maps:take(Mon, Mons) of
        {ID, NextMons} ->
            {PID, Warps} = maps:get(ID, Sectors),
            {ok, NewPID} = sj_sector_sup:create_sector(ID),
            NewMon = monitor(process, NewPID),
            NewMons = maps:put(NewMon, ID, NextMons),
            NewSectors = maps:put(ID, {NewPID, Warps}, Sectors),
            ok = repeer([ID | Warps], NewSectors),
            State#s{sectors = NewSectors, mons = NewMons};
        error ->
            Unexpected = {'DOWN', Mon, process, PID, Reason},
            ok = log(warning, "Unexpected info: ~tp", [Unexpected]),
            State
    end.


repeer(Affected, Sectors) ->
    Peer =
        fun(ID) ->
            {PID, Warps} = maps:get(ID, Sectors),
            Targets = [{S, element(1, maps:get(S, Sectors))} || S <- Warps],
            sj_sector:peer(PID, Targets)
        end,
    lists:foreach(Peer, Affected).


%%% OTP Service Functions

-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().

code_change(_, State, _) ->
    {ok, State}.


-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().

terminate(_, _) ->
    ok.



%%% Doer Functions

do_lookup(ID, #s{sectors = Sectors}) ->
    case maps:find(ID, Sectors) of
        {ok, {PID, _}} -> {ok, PID};
        error          -> error
    end.


do_sector_count(#s{sectors = none}) ->
    none;
do_sector_count(#s{sectors = Sectors}) ->
    maps:size(Sectors).


do_bigbang(Magnitude, MaxWarps, State = #s{sectors = none}) ->
    Slotted = [{I, warp_slots(MaxWarps)} || I <- lists:seq(1, Magnitude)],
    Connected = warps(Slotted, [], [], Magnitude),
    Anomalies =
        case Magnitude div 100 of
            0 -> 1;
            N -> N
        end,
    Topograph = one_way(Connected, Anomalies, Magnitude),
    ok = cache(Topograph),
    {Sectors, Mons} = manifest(Topograph, #{}, 1, Magnitude),
    ok = peer(Sectors),
    {ok, State#s{sectors = Sectors, mons = Mons}};
do_bigbang(_, _, State) ->
    {error, State}.


manifest(Topograph, Mons, ID, Magnitude) when ID > Magnitude ->
    {Topograph, Mons};
manifest(Topograph, Mons, ID, Magnitude) ->
    Warps = maps:get(ID, Topograph),
    {ok, PID} = sj_sector_sup:create_sector(ID),
    Mon = monitor(process, PID),
    NewMons = maps:put(Mon, ID, Mons),
    NewTopograph = maps:put(ID, {PID, Warps}, Topograph),
    manifest(NewTopograph, NewMons, ID + 1, Magnitude).


peer(Sectors) ->
    Iterator = maps:iterator(Sectors),
    peer(Sectors, Iterator).

peer(_, none) ->
    ok;
peer(Sectors, I) ->
    {_, {PID, Warps}, Next} = maps:next(I),
    Targets = [{S, element(1, maps:get(S, Sectors))} || S <- Warps],
    ok = sj_sector:peer(PID, Targets),
    peer(Sectors, Next).


warp_slots(N) -> lists:duplicate(rand:uniform(N), none).


one_way(Map, 0, _) ->
    Map;
one_way(Map, Anomalies, Magnitude) ->
    Target = rand:uniform(Magnitude),
    Warps = maps:get(Target, Map),
    NewWarps =
        case length(Warps) of
            1 -> Warps;
            N -> delete(rand:uniform(N), Warps)
        end,
    NewMap = maps:put(Target, NewWarps, Map),
    one_way(NewMap, Anomalies - 1, Magnitude).


delete(1, [_ | Rest]) -> Rest;
delete(N, [J | Rest]) -> [J | delete(N - 1, Rest)].


warps([{Magnitude, [none | Ws]}], Warps, Acc, Magnitude) ->
    Target = rand:uniform(Magnitude - 1),
    case lists:member(Target, Warps) of
        false ->
            BackWarps = backwarp(element(2, lists:keyfind(Target, 1, Acc)), Magnitude),
            NewAcc = lists:keystore(Target, 1, Acc, {Target, BackWarps}),
            warps([{Magnitude, Ws}], [Target | Warps], NewAcc, Magnitude);
        true ->
            warps([{Magnitude, Ws}], Warps, Acc, Magnitude)
    end;
warps([{I, [none | Ws]} | Rest], Warps, Acc, Magnitude) ->
    Target = rand:uniform(Magnitude - I) + I,
    case lists:member(Target, Warps) of
        false ->
            BackWarps = backwarp(element(2, lists:keyfind(Target, 1, Rest)), I),
            Updated = lists:keystore(Target, 1, Rest, {Target, BackWarps}),
            warps([{I, Ws} | Updated], [Target | Warps], Acc, Magnitude);
        true ->
            warps([{I, Ws} | Rest], Warps, Acc, Magnitude)
    end;
warps([{I, [W | Ws]} | Rest], Warps, Acc, Magnitude) ->
    warps([{I, Ws} | Rest], [W | Warps], Acc, Magnitude);
warps([{I, []} | Rest], Warps, Acc, Magnitude) ->
    warps(Rest, [], [{I, Warps} | Acc], Magnitude);
warps([], [], Acc, _) ->
    maps:from_list(Acc).


backwarp(Warps, I) ->
    case lists:member(I, Warps) of
        true  -> Warps;
        false -> addwarp(Warps, I)
    end.


addwarp([none | Rest], I) ->
    [I | Rest];
addwarp([I | Rest], I) ->
    Rest;
addwarp([W | Rest], I) ->
    [W | addwarp(Rest, I)];
addwarp([], _) ->
    [].


do_bigcrunch(State = #s{sectors = none}) ->
    State;
do_bigcrunch(State = #s{sectors = Sectors, mons = Mons}) ->
    Refs = maps:keys(Mons),
    ok = lists:foreach(fun demonitor/1, Refs),
    Kill = fun({PID, _}) -> supervisor:terminate_child(sj_sector_sup, PID) end,
    ok = lists:foreach(Kill, maps:values(Sectors)),
    ok = file:delete(cache_path()),
    State#s{sectors = none, mons = #{}}.


cache(Topograph) ->
    Path = cache_path(),
    Data = term_to_binary({1, Topograph}, [{compressed, 9}]),
    file:write_file(Path, Data).


cache_path() ->
    filename:join(zx_lib:path(var, "otpr", "space_jerks"), "topograph.erlbin").
