%%% @doc
%%% Space Jerks Planet Service Supervisor
%%% @end

-module(sj_planets).
-vsn("0.1.0").
-behavior(supervisor).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-export([start_link/0]).
-export([init/1]).


-spec start_link() -> {ok, pid()}.

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, none).

-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.

init(none) ->
    RestartStrategy = {rest_for_one, 1, 60},
    PlanetSup = {sj_planet_sup,
                 {sj_planet_sup, start_link, []},
                 permanent,
                 5000,
                 supervisor,
                 [sj_planet_sup]},
    PlanetMan = {sj_planet_man,
                 {sj_planet_man, start_link, []},
                 permanent,
                 5000,
                 worker,
                 [sj_planet_man]},
    Children  = [PlanetSup, PlanetMan],
    {ok, {RestartStrategy, Children}}.
