%%% @doc
%%% Space Jerks Alien Service Supervisor
%%% @end

-module(sj_aliens).
-vsn("0.1.0").
-behavior(supervisor).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-export([start_link/0]).
-export([init/1]).


-spec start_link() -> {ok, pid()}.

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, none).

-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.

init(none) ->
    RestartStrategy = {rest_for_one, 1, 60},
    AlienSup = {sj_alien_sup,
                {sj_alien_sup, start_link, []},
                permanent,
                5000,
                supervisor,
                [sj_alien_sup]},
    AlienMan = {sj_alien_man,
                {sj_alien_man, start_link, []},
                permanent,
                5000,
                worker,
                [sj_alien_man]},
    Children  = [AlienSup, AlienMan],
    {ok, {RestartStrategy, Children}}.
