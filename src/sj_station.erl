%%% @doc
%%% Space Jerks Station
%%% @end

-module(sj_station).
-vsn("0.1.0").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Ship Interface
-export([query/1, buy/4, sell/4, bid/3,
         invest/5, scan/1, rob/4, steal/5, attack/4]).
%% Service Interface
-export([new/2]).
%% gen_server
-export([start_link/2]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions

-record(s,
        {id          = 1              :: space_jerks:station_id(),
         name        = ""             :: space_jerks:station_name(),
         class       = 1              :: space_jerks:station_class(),
         last        = none           :: none | {erlang:timestamp(), ship()},
         suspicion   = none           :: none | {erlang:timestamp(), ship()},
         blacklist   = []             :: [{erlang:timestamp(), ship()}],
         fighters    = 10000          :: non_neg_integer(),
         shields     = 10000          :: non_neg_integer(),
         armor       = 10000          :: non_neg_integer(),
         credits     = 0              :: non_neg_integer(),
         negos       = []             :: [nego()],
         serial      = 1              :: serial(),
         fuel        = 0              :: non_neg_integer(),
         metal       = 0              :: non_neg_integer(),
         silica      = 0              :: non_neg_integer(),
         carbon      = 0              :: non_neg_integer(),
         fuel_v      = 10             :: non_neg_integer(),
         metal_v     = 25             :: non_neg_integer(),
         silica_v    = 60             :: non_neg_integer(),
         carbon_v    = 100            :: non_neg_integer(),
         fighter_max = 10000          :: non_neg_integer(),
         shield_max  = 10000          :: non_neg_integer(),
         armor_max   = 10000          :: non_neg_integer(),
         fuel_max    = 1000           :: non_neg_integer(),
         metal_max   = 1000           :: non_neg_integer(),
         silica_max  = 1000           :: non_neg_integer(),
         carbon_max  = 1000           :: non_neg_integer(),
         sector      = none           :: none | space_jerks:sector(),
         sector_pid  = none           :: none | pid(),
         sector_mon  = none           :: none | reference(),
         created     = os:timestamp() :: erlang:timestamp(),
         ts          = os:timestamp()}).

-record(ship,
        {id   = none :: none | space_jerks:ship_id(),
         name = ""   :: space_jerks:ship_name(),
         pid  = none :: none | pid(),
         mon  = none :: none | reference()}).

-record(nego,
        {serial   = 1              :: pos_integer(),
         ship     = none           :: none | pid(),
         type     = buy            :: buy | sell,
         resource = metal          :: space_jerks:resource(),
         amount   = 1              :: pos_integer(),
         last     = none           :: none | pos_integer(),
         bid      = 1              :: pos_integer(),
         min      = 1              :: pos_integer(),
         max      = 1              :: pos_integer(),
         patience = 2              :: pos_integer(),
         ts       = os:timestamp() :: erlang:timestamp()}).


-type state()  :: #s{}.
-type ship()   :: #ship{}.
-type nego()   :: #nego{}.
-type serial() :: pos_integer().


%%% Ship Interface

-spec query(Station) -> Available
    when Station   :: pid(),
         Available :: {Fuel   :: non_neg_integer(),
                       Metal  :: non_neg_integer(),
                       Silica :: non_neg_integer(),
                       Carbon :: non_neg_integer()}.

query(Station) ->
    gen_server:call(Station, query).
                       


-spec buy(Station, Resource, Amount, ShipName) -> {ok, Info} | {error, Reason}
    when Station  :: pid(),
         Resource :: metal | silica | carbon,
         Amount   :: pos_integer(),
         ShipName :: space_jerks:ship_name(),
         Info     :: {serial(), Offer :: pos_integer()},
         Reason   :: sold_out | refuse.

buy(Station, Resource, Amount, ShipName) ->
    gen_server:call(Station, {buy, Resource, Amount, ShipName}).


-spec sell(Station, Resource, Amount, ShipName) -> {ok, Info} | {error, Reason}
    when Station  :: pid(),
         Resource :: metal | silica | carbon,
         Amount   :: pos_integer(),
         ShipName :: space_jerks:sihp_name(),
         Info     :: {serial(), Offer :: pos_integer()},
         Reason   :: sold_out | refuse.

sell(Station, Resource, Amount, ShipName) ->
    gen_server:call(Station, {sell, Resource, Amount, ShipName}).


-spec bid(Station, Serial, Bid) -> Answer
    when Station :: pid(),
         Serial  :: serial(),
         Bid     :: cancel | pos_integer(),
         Answer  :: cancelled
                  | accept
                  | refuse
                  | {counter,   Offer :: pos_integer()}
                  | {final,     Offer :: pos_integer()}
                  | {seriously, Offer :: pos_integer()}
                  | error.

bid(Station, Serial, Amount) ->
    gen_server:call(Station, {bid, Serial, Amount}).


-spec invest(Station, Investor, ShipName, Amount, Resource) -> {ok, NewMax} | error
    when Station  :: pid(),
         Investor :: space_jerks:username(),
         ShipName :: space_jerks:ship_name(),
         Amount   :: pos_integer(),
         Resource :: space_jerks:resource(),
         NewMax   :: pos_integer().

invest(Station, Investor, ShipName, Amount, Resource) ->
    gen_server:call(Station, {invest, Investor, ShipName, Amount, Resource}).


-spec scan(Station) -> {ok, Status} | {error, Reason}
    when Station :: pid(),
         Status  :: {Credits  :: non_neg_integer(),
                     Fuel     :: non_neg_integer(),
                     Metal    :: non_neg_integer(),
                     Silica   :: non_neg_integer(),
                     Carbon   :: non_neg_integer(),
                     Fighters :: non_neg_integer(),
                     Shields  :: non_neg_integer(),
                     Armor    :: non_neg_integer()},
         Reason  :: jammed | failed.

scan(Station) ->
    gen_server:call(Station, scan).


-spec rob(Station, Amount, ShipID, ShipName) -> success | {caught, Penalty}
    when Station  :: pid(),
         Amount   :: pos_integer(),
         ShipID   :: space_jerks:ship_id(),
         ShipName :: space_jerks:ship_name(),
         Penalty  :: {Holds   :: non_neg_integer(),
                      Credits :: non_neg_integer()}.

rob(Station, Amount, ShipID, ShipName) ->
    gen_server:call(Station, {rob, Amount, ShipID, ShipName}).


-spec steal(Station, Resource, Amount, ShipID, ShipName) -> success | {caught, Penalty}
    when Station  :: pid(),
         Amount   :: pos_integer(),
         Resource :: space_jerks:resource(),
         ShipID   :: space_jerks:ship_id(),
         ShipName :: space_jerks:ship_name(),
         Penalty  :: {Holds   :: non_neg_integer(),
                      Credits :: non_neg_integer()}.

steal(Station, Resource, Amount, ShipID, ShipName) ->
    gen_server:call(Station, {steal, Resource, Amount, ShipID, ShipName}).


-spec attack(Station, ShipID, ShipName, ForceSize) -> Outcome
    when Station   :: pid(),
         ShipID    :: space_jerks:ship_id(),
         ShipName  :: space_jerks:ship_name(),
         ForceSize :: pos_integer(),
         Outcome   :: {Losses :: non_neg_integer(),
                       Damage :: {Fighters :: non_neg_integer(),
                                  Shields  :: non_neg_integer(),
                                  Armor    :: non_neg_integer()}
                               | destroyed}.

attack(Station, ShipID, ShipName, ForceSize) ->
    gen_server:call(Station, {attack, ShipID, ShipName, ForceSize}).



%%% Service Interface

-spec new(ID, Name) -> {ok, pid()}
    when ID   :: space_jerks:station_id(),
         Name :: space_jerks:station_name().

new(ID, Name) ->
    supervisor:start_child(sj_station_sup, [ID, Name]).



%%% gen_server

-spec start_link(ID, Name) -> Result
    when ID   :: space_jerks:station_id(),
         Name :: space_jerks:station_name(),
         Result :: {ok, pid()}
                 | {error, Reason :: term()}.

start_link(ID, Name) ->
    gen_server:start_link(?MODULE, {ID, Name}, []).


-spec init({ID, Name}) -> {ok, state()}
    when ID   :: space_jerks:station_id(),
         Name :: space_jerks:station_name().

init({ID, Name}) ->
    State =
        #s{id         = ID,
           name       = Name,
           class      = rand:uniform(8),
           fuel_max   = 1000 + rand:uniform(2000),
           metal_max  = 1000 + rand:uniform(2000),
           silica_max = 1000 + rand:uniform(2000),
           carbon_max = 1000 + rand:uniform(2000)},
    {ok, State}.


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().

handle_call(Message, From, State) ->
    handle_call2(Message, From, update(State)).

handle_call2(query, _, State) ->
    Response = do_query(State),
    {reply, Response, State};
handle_call2({buy, Resource, Amount, ShipName}, {ShipPID, _}, State) ->
    {Response, NewState} = do_trade(buy, Resource, Amount, ShipName, ShipPID, State),
    {reply, Response, NewState};
handle_call2({sell, Resource, Amount, ShipName}, {ShipPID, _}, State) ->
    {Response, NewState} = do_trade(sell, Resource, Amount, ShipName, ShipPID, State),
    {reply, Response, NewState};
handle_call2({bid, Serial, Amount}, _, State) ->
    {Response, NewState} = do_bid(Serial, Amount, State),
    {reply, Response, NewState};
handle_call2({invest, Investor, ShipName, Amount, Resource}, _, State) ->
    {Response, NewState} = do_invest(Investor, ShipName, Amount, Resource, State),
    {reply, Response, NewState};
handle_call2(scan, _, State) ->
    {Response, NewState} = do_scan(State),
    {reply, Response, NewState};
handle_call2({rob, Resource, Amount, ShipID, ShipName}, _, State) ->
    {Response, NewState} = do_rob(Resource, Amount, ShipID, ShipName, State),
    {reply, Response, NewState};
handle_call2({steal, Resource, Amount, ShipID, ShipName}, _, State) ->
    {Response, NewState} = do_steal(Resource, Amount, ShipID, ShipName, State),
    {reply, Response, NewState};
handle_call2({attack, ShipID, ShipName, ForceSize}, _, State) ->
    {Response, NewState} = do_attack(ShipID, ShipName, ForceSize, State),
    {reply, Response, NewState};
handle_call2(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp", [From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tp", [Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_info({'DOWN', Mon, process, PID, Reason}, State) ->
    NewState = handle_down(Mon, PID, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp", [Unexpected]),
    {noreply, State}.


handle_down(Mon, PID, Reason,
            State = #s{sector = Sector, sector_pid = PID, sector_mon = Mon,
                       negos = Negos}) ->
    ok = log(info, "Sector ~p exited with ~p. Bouncing...", [Sector, Reason]),
    ok = clear_trade(Negos),
    ok = sj_sector_man:bounce(),
    State#s{sector = none, sector_pid = none, sector_mon = none, negos = []};
handle_down(Mon, PID, Reason, State = #s{negos = Negos}) ->
    case take_nego(Mon, Negos) of
        {ok, #nego{ship = #ship{name = Name}}, NewNegos} ->
            Message = "Trading ship ~p ~p exited with ~p. Canceling negotiation.",
            ok = log(info, Message, [Name, PID, Reason]),
            State#s{negos = NewNegos};
        error ->
            Unexpected = {'DOWN', Mon, process, PID, Reason},
            ok = log(warning, "Unexpected info: ~tp", [Unexpected]),
            State
    end.

clear_trade([#nego{ship = #ship{mon = M}} | T]) ->
    true = demonitor(M),
    clear_trade(T);
clear_trade([]) ->
    ok.

take_nego(M, [#nego{ship = #ship{mon = M}} | T]) -> T;
take_nego(M, [H | T])                            -> [H | take_nego(M, T)];
take_nego(_, [])                                 -> [].


-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().

code_change(_, State, _) ->
    {ok, State}.


-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().

terminate(_, _) ->
    ok.



%%% Doer Functions

update(State = #s{fuel     = Fuel,     fuel_max    = FuelMax,
                  metal    = Metal,    metal_max   = MetalMax,
                  silica   = Silica,   silica_max  = SilicaMax,
                  carbon   = Carbon,   carbon_max  = CarbonMax,
                  fighters = Fighters, fighter_max = FighterMax,
                  shields  = Shields,  shield_max  = ShieldMax,
                  armor    = Armor,    armor_max   = ArmorMax,
                  ts       = Then}) ->
    Now = os:timestamp(),
    Elapsed = timer:now_diff(Now, Then) div 1000000,
    NewFuel     = regen(Fuel,     FuelMax,   100, Elapsed),
    NewMetal    = regen(Metal,    MetalMax,   75, Elapsed),
    NewSilica   = regen(Silica,   SilicaMax,  50, Elapsed),
    NewCarbon   = regen(Carbon,   CarbonMax,  50, Elapsed),
    NewFighters = regen(Fighters, FighterMax, 40, Elapsed),
    NewShields  = regen(Shields,  ShieldMax, 100, Elapsed),
    NewArmor    = regen(Armor,    ArmorMax,   30, Elapsed),
    State#s{fuel = NewFuel, metal = NewMetal, silica = NewSilica, carbon = NewCarbon,
            fighters = NewFighters, shields = NewShields, armor = NewArmor,
            ts = Now}.

regen(Amount, Max, _, _) when Amount >= Max ->
    Max;
regen(Amount, Max, DailyPercent, Seconds) when Amount < Max ->
    Regenerated = round(Max * ((DailyPercent * (Seconds / 86400)) / 100)),
    NewAmount = Regenerated + Amount,
    min(NewAmount, Max).


do_query(#s{fuel = Fuel, metal = Metal, silica = Silica, carbon = Carbon}) ->
    {Fuel, Metal, Silica, Carbon}.


do_trade(Type, Resource, Amount, ShipName, ShipPID,
         State = #s{serial = Serial, negos = Negos}) ->
    ShipMon = monitor(process, ShipPID),
    Ship = #ship{name = ShipName, pid = ShipPID, mon = ShipMon},
    {Nego, Status} = gen_nego(Type, Resource, Amount, Ship, Serial, State),
    case initial_bid(Nego, Status) of
        Initial = #nego{serial = Serial} ->
            NewNegos = lists:keystore(Serial, #nego.serial, Negos, Initial),
            NewState = State#s{serial = Serial + 1, negos = NewNegos},
            {{ok, Initial}, NewState};
        Refusal ->
            {{error, Refusal}, State}
    end.


gen_nego(Type, fuel,   Amount, Ship, Serial,
         #s{fuel   = Available, fuel_v   = ValuePer, fuel_max   = Max}) ->
    {#nego{serial = Serial, ship = Ship, type = Type, resource =   fuel, amount = Amount},
     {Available, ValuePer, Max}};
gen_nego(Type, metal,  Amount, Ship, Serial,
         #s{metal  = Available, metal_v  = ValuePer, metal_max  = Max}) ->
    {#nego{serial = Serial, ship = Ship, type = Type, resource =  metal, amount = Amount},
     {Available, ValuePer, Max}};
gen_nego(Type, silica, Amount, Ship, Serial,
         #s{silica = Available, silica_v = ValuePer, silica_max = Max}) ->
    {#nego{serial = Serial, ship = Ship, type = Type, resource = silica, amount = Amount},
     {Available, ValuePer, Max}};
gen_nego(Type, carbon, Amount, Ship, Serial,
         #s{carbon = Available, carbon_v = ValuePer, carbon_max = Max}) ->
    {#nego{serial = Serial, ship = Ship, type = Type, resource = carbon, amount = Amount},
     {Available, ValuePer, Max}}.


initial_bid(#nego{type = buy, amount = Amount}, {Selling, _, _}) when Amount > Selling ->
    sold_out;
initial_bid(#nego{type = sell, amount = Amount}, {Buying, _, _}) when Amount > Buying ->
    refuse;
initial_bid(Initial = #nego{type = Type, amount = Amount}, {Amount, ValuePer, Max}) ->
    FlatValue = ValuePer * Amount,
    Bid = (FlatValue + (FlatValue * spread(Type))) + offset(Type, Amount, Max, FlatValue),
    {Min, Max} = min_max(Bid),
    Patience = 1 + rand:uniform(5),
    Initial#nego{bid = round(Bid), min = round(Min), max = round(Max), patience = Patience}.

%% TODO: Allow the mean offset to be influenced by the type of resource being traded,
%%       strictness factor of the port, and maybe friendly or factional association
%%       of the trader. A whole world of things can be done here to influence balance.
spread(buy)  -> rand() * -1;
spread(sell) -> rand().

min_max(Bid) ->
    {Bid - (Bid * rand()), Bid + (Bid * rand())}.

rand() ->
    rand:normal(20, rand:uniform(20)) / 100.

offset(Type, Amount, Max, FlatValue) -> offset(Type, (Amount / Max) * FlatValue).

offset(buy,  Skew) -> Skew;
offset(sell, Skew) -> Skew * -1.


do_bid(Serial, cancel, State = #s{negos = Negos}) ->
    {cancelled, State#s{negos = lists:keydelete(Serial, #nego.serial, Negos)}};
do_bid(Serial, Bid, State = #s{negos = Negos}) ->
    case lists:keyfind(Serial, #nego.serial, Negos) of
        Selected = #nego{} -> eval_bid(Selected, Bid, State);
        false              -> {error, State}
    end.

eval_bid(Selected = #nego{last = Bid}, Bid, State) ->
    be_serious(Selected, State);
eval_bid(Selected = #nego{type = buy, min = Min, max = Max, last = Last}, Bid, State) ->
    Limit = Max + Max,
    if
        Bid >= Min andalso Bid < Limit -> accept_bid(Selected, Bid, State);
        Bid >= Limit                   -> be_serious(Selected, State);
        Bid  < Last                    -> be_serious(Selected, State);
        Bid  < Min                     -> reeval_bid(Selected, Bid, State)
    end;
eval_bid(Selected = #nego{type = sell, min = Min, max = Max, last = Last}, Bid, State) ->
    Limit = Min div 2,
    if
        Bid =< Max andalso Bid > Limit -> accept_bid(Selected, Bid, State);
        Bid =< Limit                   -> be_serious(Selected, State);
        Bid >  Last                    -> be_serious(Selected, State);
        Bid >  Max                     -> reeval_bid(Selected, Bid, State)
    end.


accept_bid(#nego{serial = Serial, type = Type, resource = Resource, amount = Amount},
           Bid,
           State = #s{negos = Negos}) ->
    NewNegos = lists:keydelete(Serial, #nego.serial, Negos),
    {accept, adjust(ops(Type), Resource, Amount, Bid, State#s{negos = NewNegos})}.

ops(buy) ->
    ResourceAdjustment = fun(A, B) -> A - B end,
    MoneyAdjustment    = fun(A, B) -> A + B end,
    {ResourceAdjustment, MoneyAdjustment};
ops(sell) ->
    ResourceAdjustment = fun(A, B) -> A + B end,
    MoneyAdjustment    = fun(A, B) -> max(A - B, 0) end,
    {ResourceAdjustment, MoneyAdjustment}.

adjust({RA, MA}, fuel,   Amount, Bid, State = #s{fuel   = Available, credits = Credits}) ->
    State#s{fuel   = RA(Available, Amount), credits = MA(Credits, Bid)};
adjust({RA, MA}, metal,  Amount, Bid, State = #s{metal  = Available, credits = Credits}) ->
    State#s{metal  = RA(Available, Amount), credits = MA(Credits, Bid)};
adjust({RA, MA}, silica, Amount, Bid, State = #s{silica = Available, credits = Credits}) ->
    State#s{silica = RA(Available, Amount), credits = MA(Credits, Bid)};
adjust({RA, MA}, carbon, Amount, Bid, State = #s{carbon = Available, credits = Credits}) ->
    State#s{carbon = RA(Available, Amount), credits = MA(Credits, Bid)}.


be_serious(#nego{bid = Offer}, State) ->
    {{seriously, Offer}, State}.


reeval_bid(Selected = #nego{type = buy, serial = Serial,
                             min = Min, bid = Offer, patience = Patience},
           Bid,
           State = #s{negos = Negos}) ->
    NewOffer = round(Offer - (((Bid / Min) * (Offer - Min)) / 4)),
    Updated = Selected#nego{patience = Patience - 1, bid = NewOffer, last = Bid},
    NewNegos = lists:keystore(Serial, #nego.serial, Negos, Updated),
    Status = case Patience =< 1 of false -> counter; true -> final end,
    {{Status, NewOffer}, State#s{negos = NewNegos}};
reeval_bid(Selected = #nego{type = sell, serial = Serial,
                             max = Max, bid = Offer, patience = Patience},
           Bid,
           State = #s{negos = Negos}) ->
    NewOffer = round(Offer + (((Bid / Max) * (Max - Offer)) / 4)),
    Updated = Selected#nego{patience = Patience - 1, bid = NewOffer, last = Bid},
    NewNegos = lists:keystore(Serial, #nego.serial, Negos, Updated),
    Status = case Patience =< 1 of false -> counter; true -> final end,
    {{Status, NewOffer}, State#s{negos = NewNegos}}.


do_invest(_, _, _, _, State) ->
    ok = log(warning, "~p/~p NIY", [?FUNCTION_NAME, ?FUNCTION_ARITY]),
    {error, State}.


do_scan(State) ->
    ok = log(warning, "~p/~p NIY", [?FUNCTION_NAME, ?FUNCTION_ARITY]),
    {error, State}.


do_rob(_, _, _, _, State) ->
    ok = log(warning, "~p/~p NIY", [?FUNCTION_NAME, ?FUNCTION_ARITY]),
    {error, State}.


do_steal(_, _, _, _, State) ->
    ok = log(warning, "~p/~p NIY", [?FUNCTION_NAME, ?FUNCTION_ARITY]),
    {error, State}.


do_attack(_, _, _, State) ->
    ok = log(warning, "~p/~p NIY", [?FUNCTION_NAME, ?FUNCTION_ARITY]),
    {error, State}.
