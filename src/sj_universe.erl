%%% @doc
%%% Space Jerks Universe Service Supervisor
%%%
%%% This is the service-level supervisor of the system. It is the parent of both the
%%% client connection handlers and the client manager (which manages the client
%%% connection handlers). This is the child of sj_sup.
%%%
%%% See: http://erlang.org/doc/apps/kernel/application.html
%%% @end

-module(sj_universe).
-vsn("0.1.0").
-behavior(supervisor).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-export([start_link/0]).
-export([init/1]).


-spec start_link() -> {ok, pid()}.
%% @private
%% This supervisor's own start function.

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, none).

-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
%% @private
%% The OTP init/1 function.

init(none) ->
    RestartStrategy = {one_for_one, 1, 60},
    Sectors   = {sj_sectors,
                 {sj_sectors, start_link, []},
                 permanent,
                 5000,
                 supervisor,
                 [sj_sectors]},
    Stations  = {sj_stations,
                 {sj_stations, start_link, []},
                 permanent,
                 5000,
                 supervisor,
                 [sj_stations]},
    Planets   = {sj_planets,
                 {sj_planets, start_link, []},
                 permanent,
                 5000,
                 supervisor,
                 [sj_planets]},
    Squadrons = {sj_squadrons,
                 {sj_squadrons, start_link, []},
                 permanent,
                 5000,
                 supervisor,
                 [sj_squadrons]},
    Ships     = {sj_ships,
                 {sj_ships, start_link, []},
                 permanent,
                 5000,
                 supervisor,
                 [sj_ships]},
    Aliens    = {sj_aliens,
                 {sj_aliens, start_link, []},
                 permanent,
                 5000,
                 supervisor,
                 [sj_aliens]},
    Children  = [Sectors, Stations, Planets, Squadrons, Ships, Aliens],
    {ok, {RestartStrategy, Children}}.
