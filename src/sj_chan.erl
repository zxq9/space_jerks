%%% @doc
%%% Space Jerks Channel
%%% @end

-module(sj_chan).
-vsn("0.1.0").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Client Interface
-export([chat/3, join/2, leave/2, list/1]).
%% gen_server
-export([start_link/3]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {id    = ""  :: space_jerks:chan(),
         users = #{} :: #{space_jerks:username() := {pid(), reference()}},
         mons  = #{} :: #{reference() := space_jerks:username()}}).


-type state() :: #s{}.


%%% Client Interface

-spec chat(Chan, User, Message) -> ok
    when Chan    :: pid(),
         User    :: space_jerks:username(),
         Message :: string().

chat(Chan, User, Message) ->
    gen_server:cast(Chan, {chat, User, Message}).


-spec join(Chan, User) -> ok
    when Chan :: pid(),
         User :: space_jerks:username().

join(Chan, User) ->
    gen_server:cast(Chan, {join, User, self()}).


-spec leave(Chan, User) -> ok
    when Chan :: pid(),
         User :: space_jerks:username().

leave(Chan, User) ->
    gen_server:cast(Chan, {leave, User}).


-spec list(Chan) -> Users
    when Chan  :: pid(),
         Users :: [space_jerks:username()].

list(Chan) ->
    gen_server:call(Chan, list).



%%% gen_server

-spec start_link(Chan, User, PID) -> Result
    when Chan   :: space_jerks:chan(),
         User   :: space_jerks:username(),
         PID    :: pid(),
         Result :: {ok, pid()}
                 | {error, Reason :: term()}.

start_link(Chan, User, PID) ->
    gen_server:start_link(?MODULE, {Chan, User, PID}, []).


-spec init({Chan, User, PID}) -> {ok, state()}
    when Chan   :: space_jerks:chan(),
         User   :: space_jerks:username(),
         PID    :: pid().

init({Chan, User, PID}) ->
    Mon = monitor(process, PID),
    State = #s{id = Chan, users = #{User => {PID, Mon}}, mons = #{Mon => User}},
    ok = sj_client:joined(PID, Chan),
    {ok, State}.


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().

handle_call(list, _, State) ->
    Response = do_list(State),
    {reply, Response, State};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "~p Unexpected call from ~tp: ~tp~n", [self(), From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_cast({chat, User, Message}, State) ->
    ok = do_chat(User, Message, State),
    {noreply, State};
handle_cast({join, User, PID}, State) ->
    NewState = do_join(User, PID, State),
    {noreply, NewState};
handle_cast({leave, User}, State) ->
    NewState = do_leave(User, State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "~p Unexpected cast: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_info({'DOWN', Mon, process, PID, Reason}, State) ->
    NewState = handle_down(Mon, PID, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "~p Unexpected info: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


handle_down(Mon, PID, Reason, State = #s{users = Users, mons = Mons}) ->
    case maps:take(Mon, Mons) of
        {User, NewMons} ->
            NewUsers = maps:remove(User, Users),
            case maps:size(NewUsers) == 0 of
                false -> State#s{users = NewUsers, mons = NewMons};
                true  -> exit(normal)
            end;
        error ->
            Unexpected = {'DOWN', Mon, process, PID, Reason},
            ok = log(warning, "Unexpected info: ~tp", [Unexpected]),
            State
    end.


-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().

code_change(_, State, _) ->
    {ok, State}.


-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().

terminate(_, _) ->
    ok.



%%% Doer Functions

do_chat(User, Message, #s{id = ID, users = Users}) ->
    Subscribers = maps:values(Users),
    Formatted =
        [sj_ansi:lt_purple(), User,
         sj_ansi:cyan(), " [#", ID, "]: ",
         sj_ansi:gray(), Message, "\r\n"],
    UTF8 = unicode:characters_to_binary(Formatted),
    Notify = fun({PID, _}) -> sj_client:show_chat(PID, UTF8) end,
    lists:foreach(Notify, Subscribers).


do_join(User, PID, State = #s{id = Chan, users = Users, mons = Mons}) ->
    case maps:is_key(User, Users) of
        false ->
            Mon = monitor(process, PID),
            NewUsers = maps:put(User, {PID, Mon}, Users),
            NewMons = maps:put(Mon, User, Mons),
            ok = sj_client:joined(PID, Chan),
            State#s{users = NewUsers, mons = NewMons};
        true ->
            State
    end.


do_leave(User, State = #s{id = Chan, users = Users, mons = Mons}) ->
    case maps:take(User, Users) of
        {{PID, Mon}, NewUsers} ->
            true = demonitor(Mon),
            NewMons = maps:remove(Mon, Mons),
            ok = sj_client:left(PID, Chan),
            case maps:size(NewUsers) == 0 of
                false -> State#s{users = NewUsers, mons = NewMons};
                true  -> exit(normal)
            end;
        error ->
            State
    end.


do_list(#s{users = Users}) ->
    maps:keys(Users).
