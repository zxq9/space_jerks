%%% @doc
%%% Space Jerks Station Manager
%%% @end

-module(sj_station_man).
-vsn("0.1.0").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Station Interface
-export([populate/1, update/1]).
%% Service Interface
-export([bigbang/2, wipe/0]).
%% gen_server
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {serial   = 1  :: pos_integer(),
         stations = [] :: [station()]}).

-record(station,
        {id          = 1              :: space_jerks:station_id(),
         name        = ""             :: space_jerks:station_name(),
         pid         = none           :: none | pid(),
         mon         = none           :: none | reference(),
         class       = 1              :: space_jerks:station_class(),
         last        = none           :: none | {erlang:timestamp(), term()},
         credits     = 0              :: non_neg_integer(),
         serial      = 1              :: pos_integer(),
         fighter_max = 10000          :: non_neg_integer(),
         shield_max  = 10000          :: non_neg_integer(),
         armor_max   = 10000          :: non_neg_integer(),
         fuel_max    = 1000           :: non_neg_integer(),
         metal_max   = 1000           :: non_neg_integer(),
         silica_max  = 1000           :: non_neg_integer(),
         carbon_max  = 1000           :: non_neg_integer(),
         sector      = none           :: none | space_jerks:sector(),
         created     = os:timestamp() :: erlang:timestamp()}).

-type state()   :: #s{}.
-type station() :: #station{}.


%%% Station Interface

-spec populate(Station) -> ok
    when Station :: station().

populate(Station) ->
    gen_server:cast(?MODULE, {populate, Station}).


-spec update(Station) -> ok
    when Station :: station().

update(Station) ->
    gen_server:cast(?MODULE, {update, Station}).


%%% Service Interface

-spec bigbang(UniverseSize, StationDensity) -> ok
    when UniverseSize   :: pos_integer(),
         StationDensity :: pos_integer(). % 1..100 as a % of stations

bigbang(UniverseSize, StationDensity) ->
    gen_server:call(?MODULE, {bigbang, UniverseSize, StationDensity}).


-spec wipe() -> ok.

wipe() ->
    gen_server:call(?MODULE, wipe).



%%% Startup Functions


-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason :: term()}.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {ok, state()}.

init(none) ->
    Path = cache_path(),
    State =
        case file:read_file(Path) of
            {ok, Bin} ->
                {1, Serial, StationData} = binary_to_term(Bin),
                Stations = lists:map(fun manifest/1, StationData),
                #s{serial = Serial, stations = Stations};
            {error, enoent} ->
                #s{}
        end,
    {ok, State}.


manifest(Selected) ->
    {ok, PID} = supervisor:start_child(sj_station_sup, Selected),
    Mon = monitor(process, PID),
    Selected#station{pid = PID, mon = Mon}.



%%% gen_server Message Handling Callbacks


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().

handle_call({bigbang, Size, Density}, _, State) ->
    NewState = do_bigbang(Size, Density, State),
    {reply, ok, NewState};
handle_call(wipe, _, State) ->
    NewState = do_wipe(State),
    {reply, ok, NewState};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "~p Unexpected call from ~tp: ~tp~n", [self(), From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_cast({populate, Station}, State) ->
    NewState = do_populate(Station, State),
    {noreply, NewState};
handle_cast({update, Station}, State) ->
    NewState = do_update(Station, State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "~p Unexpected cast: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_info({'DOWN', Mon, process, Pid, Reason}, State) ->
    NewState = handle_down(Mon, Pid, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "~p Unexpected info: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


handle_down(_, _, _, State) ->
    State.



%%% OTP Service Functions

-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().

code_change(_, State, _) ->
    {ok, State}.


-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().

terminate(_, _) ->
    ok.



%%% Doer Functions

do_bigbang(Size, Density, State = #s{serial = Serial}) ->
    Count = round((Density / 100) * Size),
    NewSerial = Serial + Count,
    NameFile = filename:join([zx_daemon:get_home(), "priv", "names.eterms"]),
    {ok, [Prefixes, Suffixes]} = file:consult(NameFile),
    PSize = length(Prefixes),
    SSize = length(Suffixes),
    Manifest =
        fun(ID) ->
            Name = random_name(ID, PSize, Prefixes, SSize, Suffixes),
            {ok, PID} = sj_station:new(ID, Name, random),
            Mon = monitor(process, PID),
            #station{id = ID, pid = PID, mon = Mon}
        end,
    Stations = lists:map(Manifest, lists:seq(Serial, NewSerial - 1)),
    State#s{serial = NewSerial, stations = Stations}.


random_name(ID, PSize, Prefixes, SSize, Suffixes) ->
    Prefix = lists:nth(rand:uniform(PSize), Prefixes),
    Suffix = lists:nth(rand:uniform(SSize), Suffixes),
    Name = Prefix ++ " " ++ Suffix,
    #station{id         = ID,
             name       = Name,
             class      = rand:uniform(8),
             fuel_max   = 1000 + rand:uniform(2000),
             metal_max  = 1000 + rand:uniform(2000),
             silica_max = 1000 + rand:uniform(2000),
             carbon_max = 1000 + rand:uniform(2000)}.


do_wipe(#s{stations = Stations}) ->
    ok = lists:foreach(fun demonitor/1, [Mon || #station{mon = Mon} <- Stations]),
    Kill = fun(#station{pid = P}) -> supervisor:terminate_child(sj_station_sup, P) end,
    ok = lists:foreach(Kill, [PID || #station{pid = PID} <- Stations]),
    ok = file:delete(cache_path()),
    #s{}.


do_populate(Updating = #station{id = ID}, State = #s{stations = Stations}) ->
    #station{pid = PID, mon = Mon} = lists:keyfind(ID, #station.id, Stations),
    Updated = Updating#station{pid = PID, mon = Mon},
    NewStations = lists:keystore(ID, #station.id, Stations, Updated),
    State#s{stations = NewStations}.


do_update(Updating, State) ->
    NewState = do_populate(Updating, State),
    ok = cache(NewState),
    NewState.


cache(#s{serial = Serial, stations = Stations}) ->
    StationData = [S#station{pid = none, mon = none} || S <- Stations],
    Path = cache_path(),
    Data = term_to_binary({1, Serial, StationData}, [{compressed, 9}]),
    file:write_file(Path, Data).


cache_path() ->
    filename:join(zx_lib:path(var, "otpr", "space_jerks"), "stations.erlbin").
