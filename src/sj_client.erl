%%% @doc
%%% Space Jerks Client
%%%
%%% An extremely naive (currently Telnet) client handler.
%%% Unlike other modules that represent discrete processes, this one does not adhere
%%% to any OTP behavior. It does, however, adhere to OTP.
%%%
%%% In some cases it is more comfortable to write socket handlers or a certain
%%% category of state machines as "pure" Erlang processes. This approach is made
%%% OTP-able by use of the proc_lib module, which is the underlying library used
%%% to write the stdlib's behaviors like gen_server, gen_statem, gen_fsm, etc.
%%%
%%% http://erlang.org/doc/design_principles/spec_proc.html
%%% @end

-module(sj_client).
-vsn("0.1.0").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").


%% Peer Interface
-export([pm/3, show_chat/2, joined/2, left/2]).
%% Ship Interface
-export([show_sector/2, show_message/2, show_action/2]).
%% User Connection
-export([start/1]).
%% OTP Compliance
-export([start_link/1, init/2]).
-export([system_continue/3, system_terminate/4,
         system_get_state/1, system_replace_state/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {socket    = none  :: none | gen_tcp:socket(),
         activity  = space :: activity(),
         user      = none  :: none | space_jerks:username(),
         chans     = #{}   :: #{space_jerks:chan() := {pid(), reference()}},
         xp        = 0     :: non_neg_integer(),
         alignment = 0     :: integer(),
         credits   = 1000  :: non_neg_integer(),
         ship      = none  :: none | pid(),
         ship_id   = none  :: space_jerks:ship_id(),
         ships     = []    :: [ship()],
         planets   = []    :: [planet()],
         squadrons = []    :: [squadron()],
         mons      = #{}   :: #{reference() := {mon_type(), ID :: term()}},
         known     = #{}   :: #{space_jerks:sector() := sector_info()}}).

-record(ship,
        {id  = 1    :: space_jerks:ship_id(),
         pid = none :: none | pid(),
         mon = none :: none | reference()}).

-record(planet,
        {id  = 1    :: space_jerks:planet_id(),
         pid = none :: none | pid(),
         mon = none :: none | reference()}).

-record(squadron,
        {id  = 1    :: space_jerks:squadron_id(),
         pid = none :: none | pid(),
         mon = none :: none | reference()}).

-record(sector,
        {name     = none           :: none | string(),
         cluster  = none           :: none | string(),
         beacon   = none           :: none | string(),
         warps    = []             :: [space_jerks:sector()],
         stations = []             :: [space_jerks:station_id()],
         planets  = []             :: [space_jerks:planet_id()],
         squadron = none           :: [space_jerks:squadron_id()],
         viewed   = os:timestamp() :: erlang:timestamp()}).


-type state()       :: #s{}.
-type ship()        :: space_jerks:ship_id()     | #ship{}.
-type planet()      :: space_jerks:planet_id()   | #planet{}.
-type squadron()    :: space_jerks:squadron_id() | #squadron{}.
-type sector_info() :: #sector{}.
-type mon_type()    :: chan | ship | planet | squadron.
-type activity()    :: space | moving | port | combat | planet.


%%% Peer Interface

-spec pm(Client, Sender, Message) -> ok
    when Client  :: pid(),
         Sender  :: space_jerks:username(),
         Message :: string().

pm(Client, Sender, Message) ->
    Client ! {show, {pm, Sender, Message}},
    ok.


-spec show_chat(Client, Message) -> ok
    when Client  :: pid(),
         Message :: binary().

show_chat(Client, Message) ->
    Client ! {show, {chat, Message}},
    ok.


-spec joined(Client, Chan) -> ok
    when Client :: pid(),
         Chan   :: space_jerks:chan().

joined(Client, Chan) ->
    Client ! {chan, {joined, Chan, self()}},
    ok.


-spec left(Client, Chan) -> ok
    when Client :: pid(),
         Chan   :: space_jerks:chan().

left(Client, Chan) ->
    Client ! {chan, {left, Chan}},
    ok.



%%% Ship Interface

-spec show_sector(Client, View) -> ok
    when Client :: pid(),
         View   :: space_jerks:sector_view().

show_sector(Client, View) ->
    Client ! {show, {sector, View}},
    ok.


-spec show_message(Client, Payload) -> ok
    when Client  :: pid(),
         Payload :: {Origin  :: space_jerks:sector(),
                     Sender  :: string(),
                     Message :: string()}.

show_message(Client, Payload) ->
    Client ! {show, {message, Payload}},
    ok.


-spec show_action(Client, Event) -> ok
    when Client :: pid(),
         Event  :: space_jerks:sector_event().

show_action(Client, Event) ->
    Client ! {show, {action, Event}},
    ok.


-spec start(ListenSocket) -> Result
    when ListenSocket :: gen_tcp:socket(),
         Result       :: {ok, pid()}
                       | {error, Reason},
         Reason       :: {already_started, pid()}
                       | {shutdown, term()}
                       | term().

start(ListenSocket) ->
    sj_client_sup:start_acceptor(ListenSocket).


-spec start_link(ListenSocket) -> Result
    when ListenSocket :: gen_tcp:socket(),
         Result       :: {ok, pid()}
                       | {error, Reason},
         Reason       :: {already_started, pid()}
                       | {shutdown, term()}
                       | term().

start_link(ListenSocket) ->
    proc_lib:start_link(?MODULE, init, [self(), ListenSocket]).


-spec init(Parent, ListenSocket) -> no_return()
    when Parent       :: pid(),
         ListenSocket :: gen_tcp:socket().

init(Parent, ListenSocket) ->
    ok = tell(info, "Listening."),
    Debug = sys:debug_options([]),
    ok = proc_lib:init_ack(Parent, {ok, self()}),
    listen(Parent, Debug, ListenSocket).



%% User Connection

-spec listen(Parent, Debug, ListenSocket) -> no_return()
    when Parent       :: pid(),
         Debug        :: [sys:dbg_opt()],
         ListenSocket :: gen_tcp:socket().

listen(Parent, Debug, ListenSocket) ->
    case gen_tcp:accept(ListenSocket) of
        {ok, Socket} ->
            {ok, _} = start(ListenSocket),
            {ok, {IP, Port}} = inet:peername(Socket),
            Addr = inet:ntoa(IP),
            ok = tell(info, "Connection accepted from: ~ts:~w", [Addr, Port]),
            ok = sj_client_man:enroll(),
            State = #s{socket = Socket},
            ok = tx(Socket, sj_ansi:greet_screen()),
            greet(Parent, Debug, State);
        {error, closed} ->
            ok = tell(info, "Retiring: Listen socket closed."),
            exit(normal)
     end.


greet(Parent, Debug, State = #s{socket = Socket}) ->
    case prompt(Parent, Debug, State, "What's your name, jerk?") of
        "" ->
            ok = tx(Socket, ["Huh? Let's try that again..."]),
            greet(Parent, Debug, State);
        Name ->
            check_name(Parent, Debug, State#s{user = Name})
    end.


check_name(Parent, Debug, State = #s{user = Name}) ->
    case sj_client_man:load_user(Name) of
        {ok, PassData} -> password(Parent, Debug, State, PassData);
        error          -> new_user(Parent, Debug, State)
    end.


password(Parent, Debug, State = #s{socket = Socket}, PassData) ->
    case prompt(Parent, Debug, State, "Enter your password:") of
        "" ->
            Message =
                "Look, passwords aren't just \"social constructs\". "
                "Let's try that again",
            ok = tx(Socket, [Message]),
            password(Parent, Debug, State, PassData);
        String ->
            Passphrase = list_to_binary(String),
            check_password(Parent, Debug, State, Passphrase, PassData)
    end.


check_password(Parent, Debug, State = #s{user = Name, socket = Socket},
               Passphrase, PassData = {Type, Salt, Hash}) ->
    case crypto:hash(Type, <<Salt/binary, Passphrase/binary>>) =:= Hash of
        true ->
            NewState = #s{ship = Ship} = return(Parent, Debug, State),
            ok = sj_ship:embark(Ship, {user, Name, self()}),
            welcome(Parent, Debug, NewState);
        false ->
            ok = tx(Socket, ["Hmmm?"]),
            ok = dot(Socket, 5),
            ok = tx(Socket, ["that didn't come out right. Let's try that again."]),
            password(Parent, Debug, State, PassData)
    end.


welcome(Parent, Debug, State = #s{user = Name, socket = Socket}) ->
    Message = [["Welcome ", sj_ansi:lt_green(), Name, sj_ansi:gray(), "!"]],
    ok = tx(Socket, Message),
    loop(Parent, Debug, State).


new_user(Parent, Debug, State = #s{socket = Socket}) ->
    case prompt(Parent, Debug, State, "Are you a new player? [Y/N]") of
        "y" -> create_user(Parent, Debug, State);
        "Y" -> create_user(Parent, Debug, State);
        "n" -> greet(Parent, Debug, State);
        "N" -> greet(Parent, Debug, State);
        _   ->
            ok = tx(Socket, ["Um... what? Once again..."]),
            new_user(Parent, Debug, State)
    end.


create_user(Parent, Debug, State = #s{socket = Socket}) ->
    Prompt = "Enter the passphrase you would like to use:",
    case prompt(Parent, Debug, State, Prompt) of
        "" ->
            Message =
                "Having an empty head is no crime around here, "
                "but you can't have an empty passphrase.",
            ok = tx(Socket, [Message]),
            create_user(Parent, Debug, State);
        String ->
            Salt = crypto:strong_rand_bytes(64),
            Password = list_to_binary(String),
            Type = sha512,
            Hash = crypto:hash(Type, <<Salt/binary, Password/binary>>),
            PassData = {Type, Salt, Hash},
            ship_name(Parent, Debug, State, PassData)
    end.


ship_name(Parent, Debug, State = #s{socket = Socket}, PassData) ->
    Prompt = "What do you want to name your first ship?",
    case prompt(Parent, Debug, State, Prompt) of
        "" ->
            Message = "You have to call it SOMETHING...",
            ok = tx(Socket, [Message]),
            ship_name(Parent, Debug, State, PassData);
        ShipName ->
            beget_user(Parent, Debug, State, ShipName, PassData)
    end.


beget_user(Parent, Debug, State = #s{user = Name, socket = Socket, mons = Mons},
           ShipName, PassData) ->
    case sj_client_man:create_user(Name, PassData) of
        ok ->
            {ok, ShipID, ShipPID} = sj_ship_man:christen("Hauler", "Monda", ShipName),
            ok = sj_client_man:login(Name),
            LocID = 1,
            {ok, LocPID} = sj_sector_man:lookup(LocID),
            ok = sj_ship:claim(ShipPID, {user, Name}),
            ok = sj_ship:embark(ShipPID, {user, Name, self()}),
            ShipMon = monitor(process, ShipPID),
            NewMons = maps:put(ShipMon, {ship, ShipID}, Mons),
            ok = sj_ship:warp(ShipPID, LocPID),
            NewState =
                State#s{ship_id = ShipID,
                        ship    = ShipPID,
                        ships   = [#ship{id = ShipID, pid = ShipPID, mon = ShipMon}],
                        mons    = NewMons},
            ok = do_cache(State),
            welcome(Parent, Debug, NewState);
        error ->
            Message =
                ["Whoa! You're not going to believe this, but some other jerk nabbed "
                 "the name you are trying to register JUST NOW!",
                 "Only a loser would let that slide.",
                 "Best register another name straight away so you can go get him."],
            ok = tx(Socket, Message),
            greet(Parent, Debug, State#s{user = none})
    end.


return(Parent, Debug, State = #s{user = Name}) ->
    NextState = #s{ship_id = SID, ships = Ss, planets = Ps, squadrons = Sqs} =
        load(State),
    Ships     = acquire(sj_ship_man,     Ss,  Name),
    Planets   = acquire(sj_planet_man,   Ps,  Name),
    Squadrons = acquire(sj_squadron_man, Sqs, Name),
    Rectified = rectify(NextState#s{ships     = Ships,
                                    planets   = Planets,
                                    squadrons = Squadrons}),
    NewState =
        case lists:keyfind(SID, #ship.id, Ships) of
            #ship{pid = PID} -> Rectified#s{ship = PID};
            false            -> grant_ship(Parent, Debug, Rectified)
        end,
    ok = sj_client_man:login(Name),
    NewState.

%% NOTE:
%%  This is a slightly dodgy bit of code here, but prevents me from having to
%%  write basically the same thing three times. The reason this works is that the
%%  record definitions for #ship{}, #planet{} and #squadron{} are the same order and
%%  arity. This will probably break in the future, but for now... meh.
acquire(ManMod, Stuff, Name) ->
    Owner = {user, Name},
    MaybeLoad =
        fun({Type, ID, _, _}, List) ->
            case ManMod:lookup_if_mine(ID, Owner) of
                {ok, PID} ->
                    Mon = monitor(process, PID),
                    Thing = {Type, ID, PID, Mon},
                    lists:keystore(ID, 2, List, Thing);
                error ->
                    lists:keydelete(ID, 2, List)
            end
        end,
    lists:foldl(MaybeLoad, [], Stuff).

rectify(State = #s{ships = Ss, planets = Ps, squadrons = Sqs, mons = Mons}) ->
    R = fun({T, ID, _, M}, Ms) -> maps:put(M, {T, ID}, Ms) end,
    NewMons = lists:foldl(R, lists:foldl(R, lists:foldl(R, Mons, Ss), Ps), Sqs),
    State#s{mons = NewMons}.


load(State = #s{socket = Socket, chans = Chans, mons = Mons}) ->
    Path = cache_path(State),
    {ok, Bin} = file:read_file(Path),
    {1, Loaded} = binary_to_term(Bin),
    Loaded#s{socket = Socket, chans = Chans, mons = Mons}.


grant_ship(Parent, Debug, State = #s{socket = Socket}) ->
    Message =
        ["Oh noes! You don't have a ship. Was it destroyed while you were away?",
         "Anyway, we'll have to fix you up with a new one."],
    ok = tx(Socket, Message),
    new_ship(Parent, Debug, State, 1).


new_ship(Parent, Debug,
         State = #s{user = Name, ships = Ships, socket = Socket}, LocID) ->
    Prompt = "What would you like to name your ship?",
    case prompt(Parent, Debug, State, Prompt) of
        "" ->
            tx(Socket, ["Come on, you have to call it *something*, don't you?"]),
            new_ship(Parent, Debug, State, LocID);
        ShipName ->
            {ok, ShipID, ShipPID} = sj_ship_man:christen("Hauler", "Monda", ShipName),
            ok = sj_ship:claim(ShipPID, {user, Name}),
            ShipMon = monitor(process, ShipPID),
            {ok, LocPID} = sj_sector_man:lookup(LocID),
            ok = sj_ship:warp(ShipPID, LocPID),
            Ship = #ship{id = ShipID, pid = ShipPID, mon = ShipMon},
            State#s{ships = lists:keystore(ShipID, #ship.id, Ships, Ship)}
    end.


-spec loop(Parent, Debug, State) -> no_return()
    when Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().
%% @private
%% The service loop itself. This is the service state. The process blocks on receive
%% of Erlang messages, TCP segments being received themselves as Erlang messages.

loop(Parent, Debug, State = #s{socket = Socket}) ->
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, Message} ->
            ok = dispatch(string:trim(Message), State),
            loop(Parent, Debug, State);
        {show, Stuff} ->
            NewState = do_show(State, Stuff),
            ok = prompt(NewState),
            loop(Parent, Debug, NewState);
        {chan, Action} ->
            NewState = do_chan(Action, State),
            ok = prompt(NewState),
            loop(Parent, Debug, NewState);
        prompt ->
            ok = prompt(State),
            loop(Parent, Debug, State);
        {tcp_closed, Socket} ->
            ok = tell(info, "Socket closed, retiring."),
            exit(normal);
        {'DOWN', Mon, process, PID, Reason} ->
            NewState = handle_down(Mon, PID, Reason, State),
            loop(Parent, Debug, NewState);
        {system, From, Request} ->
            S = {?FUNCTION_NAME, State},
            sys:handle_system_msg(Request, From, Parent, ?MODULE, Debug, S);
        Unexpected ->
            ok = tell(info, "Unexpected message: ~tp", [Unexpected]),
            loop(Parent, Debug, State)
    end.


handle_down(Mon, PID, Reason,
            State = #s{chans = Chans, ships = Ships,
                       planets = Planets, squadrons = Squadrons,
                       mons = Mons}) ->
    case maps:take(Mon, Mons) of
        {{chan, ID}, NewMons} ->
            NewChans = maps:remove(ID, Chans),
            State#s{chans = NewChans, mons = NewMons};
        {{ship, ID}, NewMons} ->
            NewShips = lists:keydelete(ID, #ship.id, Ships),
            State#s{ships = NewShips, mons = NewMons};
        {{planet, ID}, NewMons} ->
            NewPlanets = lists:keydelete(ID, #planet.id, Planets),
            State#s{planets = NewPlanets, mons = NewMons};
        {{squadron, ID}, NewMons} ->
            NewSquadrons = lists:keydelete(ID, #squadron.id, Squadrons),
            State#s{squadrons = NewSquadrons, mons = NewMons};
        error ->
            Unexpected = {'DOWN', Mon, process, PID, Reason},
            ok = log(warning, "Unexpected info: ~tp", [Unexpected]),
            State
    end.



%% OTP Compliance

-spec system_continue(Parent, Debug, State) -> no_return()
    when Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().
%% @private
%% The function called by the OTP internal functions after a system message has been
%% handled. If the worker process has several possible states this is one place
%% resumption of a specific state can be specified and dispatched.

system_continue(Parent, Debug, {F, State}) ->
    F(Parent, Debug, State).


-spec system_terminate(Reason, Parent, Debug, State) -> no_return()
    when Reason :: term(),
         Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().
%% @private
%% Called by the OTP inner bits to allow the process to terminate gracefully.
%% Exactly when and if this is callback gets called is specified in the docs:
%% See: http://erlang.org/doc/design_principles/spec_proc.html#msg

system_terminate(Reason, _Parent, _Debug, _State) ->
    exit(Reason).



-spec system_get_state(State) -> {ok, State}
    when State :: state().
%% @private
%% This function allows the runtime (or anything else) to inspect the running state
%% of the worker process at any arbitrary time.

system_get_state(State) -> {ok, State}.


-spec system_replace_state(StateFun, State) -> {ok, NewState, State}
    when StateFun :: fun(),
         State    :: state(),
         NewState :: term().
%% @private
%% This function allows the system to update the process state in-place. This is most
%% useful for state transitions between code types, like when performing a hot update
%% (very cool, but sort of hard) or hot patching a running system (living on the edge!).

system_replace_state(StateFun, State) ->
    {ok, StateFun(State), State}.



%%% Doer Functions


dispatch("", #s{ship = Ship}) ->
    sj_ship:view(Ship);
dispatch("! " ++ Message, #s{ship = Ship}) ->
    sj_ship:radio(Ship, 1, Message);
dispatch([$!, Range, 32 | Message], #s{ship = Ship})
        when Range >= $1 andalso Range =< $9 ->
    sj_ship:radio(Ship, Range - $0, Message);
dispatch([$@ | Line], #s{user = Name, socket = Socket}) ->
    {Target, Message} = object(Line, []),
    case sj_client_man:lookup(Target) of
        {ok, PID} -> sj_client:pm(PID, Name, Message);
        error     -> tx(Socket, ["That user is not available."])
    end;
dispatch("." ++ Sector, State = #s{socket = Socket}) ->
    case numeric(Sector) of
        true ->
            Destination = list_to_integer(Sector),
            move(Destination, State);
        false ->
            derp(Socket)
    end;
dispatch("# " ++ Line, #s{user = Name, chans = Chans, socket = Socket}) ->
    case string:lexemes(Line, " ") of
        ["list"]        -> chan_list(Socket);
        ["join",  Chan] -> chan_join(Chan, Name, Chans, Socket);
        ["leave", Chan] -> chan_leave(Chan, Name, Chans, Socket);
        ["users", Chan] -> chan_users(Chan, Chans, Socket);
        _               -> derp(Socket)
    end;
dispatch("?", #s{socket = Socket}) ->
    tx(Socket, help());
dispatch([$# | Line], #s{user = Name, chans = Chans, socket = Socket}) ->
    {Chan, Message} = object(Line, []),
    case maps:find(Chan, Chans) of
        {ok, {ChanPID, _}} -> sj_chan:chat(ChanPID, Name, Message);
        error              -> tx(Socket, ["You aren't in that channel."])
    end;
dispatch("color", #s{socket = Socket}) ->
    tx(Socket, sj_ansi:color_check());
dispatch("QUIT", State = #s{user = Name, xp = XP, alignment = A, socket = Socket}) ->
    ok = sj_stats:record(Name, XP, A),
    ok = sj_stats:cache(),
    ok = do_cache(State),
    ok = gen_tcp:send(Socket, "Bye!\r\n"),
    ok = gen_tcp:shutdown(Socket, read_write),
    exit(normal);
dispatch(_, #s{socket = Socket}) ->
    derp(Socket).


object([32 | Line], Acc) ->
    object_strip(Line, lists:reverse(Acc));
object([Char | Line], Acc) ->
    object(Line, [Char | Acc]).

object_strip([32 | Line], Object) ->
    object_strip(Line, Object);
object_strip(Line, Object) ->
    {Object, Line}.


numeric("")     -> false;
numeric(String) -> lists:all(fun numeral/1, String).

numeral(C) -> C >= $0 andalso C =< $9.


move(Destination, #s{ship = Ship, socket = Socket}) ->
    case sj_ship:move(Ship, Destination) of
        ok    -> ok;
        error -> tx(Socket, ["That sector is not adjacent."])
    end.


do_show(State, {sector, View}) ->
    do_show_sector(State, View);
do_show(State, {action, Event}) ->
    ok = do_show_action(State, Event),
    State;
do_show(State, {message, Payload}) ->
    ok = do_show_message(State, Payload),
    State;
do_show(State, {pm, Sender, Message}) ->
    ok = do_show_pm(State, Sender, Message),
    State;
do_show(State, {chat, Message}) ->
    ok = do_show_chat(State, Message),
    State.


do_show_sector(State = #s{socket = Socket}, View) ->
    NewState = #s{known = Known} = known(State, View),
    Lines = display_sector(Known, View),
    ok = tx(Socket, ["---" | Lines]),
    NewState.

known(State = #s{known = Sectors},
      {ID, Name, Cluster, Beacon, Warps, Stations, Squadron, _}) ->
    Viewed = 
        #sector{name     = Name,
                cluster  = Cluster,
                beacon   = Beacon,
                warps    = Warps,
                stations = Stations,
                squadron = Squadron},
    State#s{known = maps:put(ID, Viewed, Sectors)}.


do_show_action(#s{socket = Socket}, {arrival, ShipName, Pilot, Owner}) ->
    Line =
        [sj_ansi:gray(), "The ", sj_ansi:cyan(), io_lib:format("~tp", [ShipName]),
         sj_ansi:yellow(), " arrived ", sj_ansi:gray(), "in the sector ",
         format_pilot(Pilot, Owner)],
    tx(Socket, [Line]);
do_show_action(#s{socket = Socket}, {departure, ShipName, Pilot}) ->
    Line =
        [sj_ansi:gray(), "The ", sj_ansi:cyan(), io_lib:format("~tp", [ShipName]),
         sj_ansi:yellow(), " departed ", sj_ansi:gray(), "the sector ",
         format_pilot(Pilot)],
    tx(Socket, [Line]);
do_show_action(#s{socket = Socket}, Unknown) ->
    Line =
        [sj_ansi:yellow(), "Unknown event: ",
         sj_ansi:lt_red(), io_lib:format("~p", [Unknown])],
    tx(Socket, [Line]).


do_show_message(#s{socket = Socket}, {OriginLoc, Sender, Message}) ->
    Line =
        [sj_ansi:lt_cyan(), Sender, sj_ansi:lt_purple(),
         "[", integer_to_list(OriginLoc), "]: ",
         sj_ansi:gray(), Message],
    tx(Socket, [Line]).


do_show_pm(#s{socket = Socket}, Sender, Message) ->
    Line = [sj_ansi:lt_red(), Sender, ": ", sj_ansi:gray(), Message],
    tx(Socket, [Line]).


do_show_chat(#s{socket = Socket}, Message) ->
    gen_tcp:send(Socket, Message).


chan_list(Socket) ->
    Lines = [[sj_ansi:white(), "Channels:", sj_ansi:lt_green()] | sj_chan_man:list()],
    tx(Socket, Lines).


chan_join(Chan, UserName, Chans, Socket) ->
    case maps:is_key(Chan, Chans) of
        false -> chan_join2(Chan, UserName);
        true  -> tx(Socket, ["You're already in that channel."])
    end.

chan_join2(Chan, UserName) ->
    case sj_chan_man:lookup(Chan) of
        {ok, ChanPID} -> sj_chan:join(ChanPID, UserName);
        error         -> sj_chan_man:create(Chan, UserName)
    end.


chan_leave(Chan, UserName, Chans, Socket) ->
    case maps:find(Chan, Chans) of
        {ok, {ChanPID, _}} -> sj_chan:leave(ChanPID, UserName);
        error              -> tx(Socket, ["You're not in that channel."])
    end.


chan_users(Chan, Chans, Socket) ->
    case maps:find(Chan, Chans) of
        {ok, {ChanPID, _}} -> chan_users3(Chan, ChanPID, Socket);
        error              -> chan_users2(Chan, Socket)
    end.

chan_users2(Chan, Socket) ->
    case sj_chan_man:lookup(Chan) of
        {ok, {ChanPID, _}} -> chan_users3(Chan, ChanPID, Socket);
        error              -> tx(Socket, ["That channel does not exist."])
    end.

chan_users3(Chan, ChanPID, Socket) ->
    Lines = [[sj_ansi:white(), "User in channel ",
              sj_ansi:lt_purple(), "#", Chan, sj_ansi:white(), ":", sj_ansi:lt_green()]
             | sj_chan:list(ChanPID)],
    tx(Socket, Lines).


do_chan({joined, Chan, PID}, State = #s{chans = Chans, mons = Mons}) ->
    Mon = monitor(process, PID),
    NewChans = maps:put(Chan, {PID, Mon}, Chans),
    NewMons = maps:put(Mon, {chan, Chan}, Mons),
    State#s{chans = NewChans, mons = NewMons};
do_chan({left, Chan}, State = #s{chans = Chans, mons = Mons}) ->
    case maps:take(Chan, Chans) of
        {{_, Mon}, NewChans} ->
            NewMons = maps:remove(Mon, Mons),
            State#s{chans = NewChans, mons = NewMons};
        error ->
            State
    end.


display_sector(Known, {ID, Name, Cluster, Beacon, Warps, Stations, Squadron, Ships}) ->
    Sorted = lists:sort(Warps),
    Line = [sj_ansi:white(), "Warps: ", sj_ansi:purple(),
            "[", format_warps(Known, Sorted), sj_ansi:purple(), "]"],
    format_sector(ID, Name, Cluster, Beacon, Stations, Squadron, Ships, [Line]).

format_sector(ID, Name, Cluster, Beacon, Stations, Squadron, [], Lines) ->
    format_sector(ID, Name, Cluster, Beacon, Stations, Squadron, Lines);
format_sector(ID, Name, Cluster, Beacon, Stations, Squadron, Ships, Lines) ->
    Header = [sj_ansi:white(), "Ships:"],
    NewLines = [Header | format_ships(Ships, Lines)],
    format_sector(ID, Name, Cluster, Beacon, Stations, Squadron, NewLines).

format_sector(ID, Name, Cluster, Beacon, Stations, none, Lines) ->
    format_sector(ID, Name, Cluster, Beacon, Stations, Lines);
format_sector(ID, Name, Cluster, Beacon, Stations, Squadron, Lines) ->
    Header = [sj_ansi:yellow(), "Squadron: "],
    Line = format_squadron(Squadron),
    format_sector(ID, Name, Cluster, Beacon, Stations, [[Header | Line] | Lines]).

format_sector(ID, Name, Cluster, Beacon, [], Lines) ->
    format_sector(ID, Name, Cluster, Beacon, Lines);
format_sector(ID, Name, Cluster, Beacon, Stations, Lines) ->
    Header = [sj_ansi:white(), "Stations:"],
    NewLines = format_stations(Stations, Lines),
    format_sector(ID, Name, Cluster, Beacon, [Header | NewLines]).

format_sector(ID, Name, Cluster, none, Lines) ->
    format_sector(ID, Name, Cluster, Lines);
format_sector(ID, Name, Cluster, Beacon, Lines) ->
    Line = [sj_ansi:white(), "Beacon: ", sj_ansi:lt_red(), Beacon],
    format_sector(ID, Name, Cluster, [Line | Lines]).

format_sector(ID, Name, none, Lines) ->
    format_sector(ID, Name, Lines);
format_sector(ID, Name, Cluster, Lines) ->
    Line = [sj_ansi:white(), "Cluster: ", sj_ansi:lt_green(), Cluster],
    format_sector(ID, Name, [Line | Lines]).

format_sector(ID, none, Lines) ->
    format_sector(ID, Lines);
format_sector(ID, Name, Lines) ->
    Line = [sj_ansi:white(), "Name: ", sj_ansi:yellow(), Name],
    format_sector(ID, [Line | Lines]).

format_sector(ID, Lines) ->
    Line = [sj_ansi:lt_red(), "Sector: ", sj_ansi:yellow(), io_lib:format("~w", [ID])],
    [Line | Lines].


format_warps(Known, [ID | Rest]) ->
    case maps:is_key(ID, Known) of
        true ->
            [sj_ansi:yellow(), io_lib:format(" ~w ", [ID]) | format_warps(Known, Rest)];
        false ->
            [sj_ansi:red(),
             " (", sj_ansi:lt_red(), io_lib:format("~w", [ID]),
             sj_ansi:red(), ") "
              | format_warps(Known, Rest)]
    end;
format_warps(_, []) ->
    [].

format_ships([{Name, Type, Escort, Pilot, Owner} | Rest], Lines) ->
    Line = [format_shipname(Name, Type, Escort), " ", format_pilot(Pilot, Owner)],
    format_ships(Rest, [Line | Lines]);
format_ships([], Lines) ->
    Lines.

format_shipname(Name, Type, Escort) ->
    [sj_ansi:cyan(), io_lib:format("    ~tp ", [Name]),
     sj_ansi:purple(), "(",
     sj_ansi:lt_purple(), io_lib:format("~ts", [Type]),
     sj_ansi:purple(), ",",
     sj_ansi:yellow(), io_lib:format("~w", [Escort]),
     sj_ansi:purple(), ")",
     sj_ansi:gray()].

format_pilot({alien, Name, _}, _) ->
    [sj_ansi:gray(), "piloted by ", sj_ansi:lt_purple(), io_lib:format("~ts", [Name])];
format_pilot({user, Name, _}, _) ->
    [sj_ansi:gray(), "piloted by ", sj_ansi:lt_red(), io_lib:format("~ts", [Name])];
format_pilot({alien, Name}, _) ->
    [sj_ansi:gray(), "piloted by ", sj_ansi:lt_purple(), io_lib:format("~ts", [Name])];
format_pilot({user, Name}, _) ->
    [sj_ansi:gray(), "piloted by ", sj_ansi:lt_red(), io_lib:format("~ts", [Name])];
format_pilot(none, {corp, Owner}) ->
    [sj_ansi:gray(), "(owned by ", sj_ansi:purple(), io_lib:format("~ts", [Owner]),
     sj_ansi:gray(), ")"];
format_pilot(none, {user, Owner}) ->
    [sj_ansi:gray(), "(owned by ", sj_ansi:purple(), io_lib:format("~ts", [Owner]),
     sj_ansi:gray(), ")"].

format_pilot({alien, Name, _}) ->
    [sj_ansi:gray(), "piloted by ", sj_ansi:lt_purple(), io_lib:format("~ts", [Name])];
format_pilot({user, Name, _}) ->
    [sj_ansi:gray(), "piloted by ", sj_ansi:lt_red(), io_lib:format("~ts", [Name])];
format_pilot(none) ->
    "".

format_squadron({Size, Mode, Owner}) ->
    Hazard =
        case Mode of
            attack    -> [sj_ansi:lt_red(), "attack"];
            toll      -> [sj_ansi:yellow(), "toll"];
            defensive -> [sj_ansi:white(), "defensive"]
        end,
    Responsible =
        case Owner of
            none          -> [sj_ansi:yellow(), "NOBODY"];
            rogue         -> [sj_ansi:lt_red(), "Rogues!"];
            {alien, Name} -> Name;
            {user, Name}  -> [sj_ansi:lt_purple(), Name];
            {corp, Name}  -> [sj_ansi:lt_red(), Name]
        end,
    [sj_ansi:lt_red(), io_lib:format("~w ", [Size]),
     sj_ansi:purple(), "[", Hazard, sj_ansi:purple(),
     "] (Owned by ", Responsible, sj_ansi:purple(), ")"].

format_stations([{Name, Type} | Rest], Lines) ->
    Port =
        case Type of
            1        -> [sj_ansi:green(),"BBB"];
            2        -> [sj_ansi:green(),"BB",sj_ansi:cyan(),"S"];
            3        -> [sj_ansi:green(),"B",sj_ansi:cyan(), "SS"];
            4        -> [sj_ansi:cyan(), "SSS"];
            5        -> [sj_ansi:cyan(), "SS",sj_ansi:green(),"B"];
            6        -> [sj_ansi:cyan(), "S",sj_ansi:green(), "BB"];
            7        -> [sj_ansi:green(),"B",sj_ansi:cyan(),  "S", sj_ansi:green(),"B"];
            8        -> [sj_ansi:cyan(), "S",sj_ansi:green(), "B", sj_ansi:cyan(), "S"];
            shipyard -> [sj_ansi:cyan(),  "Shipyard"];
            stardock -> [sj_ansi:cyan(),  "Stardock"]
        end,
    Line =
        [sj_ansi:yellow(), io_lib:format("    ~ts", [Name]),
         sj_ansi:purple(), " (", Port, sj_ansi:purple(), ")"],
    format_stations(Rest, [Line | Lines]);
format_stations([], Lines) ->
    Lines.


do_cache(State = #s{ships = Ss, planets = Ps, squadrons = Sqs}) ->
    ok = sj_ship_man:cache(),
    CleanShips     = lists:map(fun(S) ->     S#ship{pid = none, mon = none} end, Ss),
    CleanPlanets   = lists:map(fun(P) ->   P#planet{pid = none, mon = none} end, Ps),
    CleanSquadrons = lists:map(fun(S) -> S#squadron{pid = none, mon = none} end, Sqs),
    CleanState =
        State#s{socket    = none,
                chans     = #{},
                ship      = none,
                ships     = CleanShips,
                planets   = CleanPlanets,
                squadrons = CleanSquadrons,
                mons      = #{}},
    Version = 1,
    Path = cache_path(State),
    Data = term_to_binary({Version, CleanState}, [{compressed, 9}]),
    file:write_file(Path, Data).


cache_path(#s{user = Name}) ->
    File = "user-" ++ Name ++ ".erlbin",
    filename:join(zx_lib:path(var, "otpr", "space_jerks"), File).


%%% TCP

tx(Socket, Lines) ->
    Message = unicode:characters_to_binary(prep(Lines)),
    gen_tcp:send(Socket, Message).


prep([Line | Lines]) -> [Line, "\r\n" | prep(Lines)];
prep([])             -> [].


prompt(#s{socket = Socket}) ->
    Line = 
        [sj_ansi:purple(), "[", sj_ansi:yellow(), timestring(), sj_ansi:purple(), "]",
         sj_ansi:lt_purple(), " > ", sj_ansi:gray()],
    gen_tcp:send(Socket, unicode:characters_to_binary(Line)).

timestring() ->
    {{Year, Mon, Day}, {Hour, Min, Sec}} = calendar:now_to_local_time(os:timestamp()),
    Date = io_lib:format("~w-~w-~w", [Year, Mon, Day]),
    Time = io_lib:format("~w:~w:~w", [Hour, Min, Sec]),
    [sj_ansi:yellow(), Date, sj_ansi:purple(), "T", sj_ansi:yellow(), Time].


prompt(Parent, Debug, State = #s{socket = Socket}, Text) ->
    Message = [Text, " "],
    ok = gen_tcp:send(Socket, Message),
    rx(Parent, Debug, State).


rx(Parent, Debug, State = #s{socket = Socket}) ->
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, Message} ->
            string:trim(Message);
        {tcp_closed, Socket} ->
            ok = tell(info, "Socket closed, retiring."),
            exit(normal);
        {system, From, Request} ->
            S = {?FUNCTION_NAME, State},
            sys:handle_system_msg(Request, From, Parent, ?MODULE, Debug, S);
        Unexpected ->
            ok = tell(warning, "Unexpected message: ~tp", [Unexpected]),
            rx(Parent, Debug, State)
    end.


dot(Socket, 0) ->
    gen_tcp:send(Socket, ".");
dot(Socket, Count) ->
    ok = timer:sleep(1000),
    ok = gen_tcp:send(Socket, "."),
    dot(Socket, Count - 1).


help() ->
    self() ! prompt,
    ["There will be a help screen.",
     "It will be awesome.",
     "But not yet."].


derp(Socket) ->
    tx(Socket, ["Arglebargle, glop-glyf!?!"]).
