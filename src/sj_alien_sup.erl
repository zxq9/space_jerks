%%% @doc
%%% Space Jerks Alien Supervisor
%%% @end

-module(sj_alien_sup).
-vsn("0.1.0").
-behaviour(supervisor).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").


-export([start_link/0]).
-export([init/1]).



-spec start_link() -> {ok, pid()}.

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, none).


-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.

init(none) ->
    RestartStrategy = {simple_one_for_one, 1, 60},
    Alien = {sj_alien,
              {sj_alien, start_link, []},
              temporary,
              brutal_kill,
              worker,
              [sj_alien]},
    {ok, {RestartStrategy, [Alien]}}.
