%%% @doc
%%% Space Jerks
%%% @end

-module(space_jerks).
-vsn("0.1.0").
-behavior(application).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-export([listen/1, ignore/0, bigbang/1, bigcrunch/0]).
-export([start/0, stop/0]).
-export([start/2, stop/1]).

-export_type([username/0, chan/0, corp_id/0, sector/0,
              station_id/0, station_name/0, station_type/0,
              ship_id/0, ship_type/0, ship_name/0, ship_make/0,
              planet_id/0, planet_name/0, planet_type/0,
              squadron_id/0, squadron_mode/0,
              owner/0, alien_name/0, pilot/0,
              sector_view/0, sector_event/0,
              resource/0, station_class/0]).

-include("$zx_include/zx_logger.hrl").


-type username()       :: string().
-type chan()           :: string().
-type corp_id()        :: pos_integer().
-type sector()         :: pos_integer().
-type station_id()     :: pos_integer().
-type station_name()   :: string().
-type station_type()   :: shipyard | stardock | 1..8.
-type ship_id()        :: pos_integer().
-type ship_type()      :: string().
-type ship_name()      :: string().
-type ship_make()      :: string().
-type planet_id()      :: pos_integer().
-type planet_name()    :: string().
-type planet_type()    :: string().
-type squadron_id()    :: pos_integer().
-type squadron_mode()  :: defensive | offensive | toll.
-type owner()          :: none | rogue | {user, username()} | {corp, corp_id()}.
-type alien_name()     :: string().
-type pilot()          :: {Type       :: user | alien,
                           Name       :: username() | alien_name(),
                           Controller :: none | pid()}.
-type sector_view()   :: {ID       :: sector(),
                          Name     :: none | string(),
                          Cluster  :: none | string(),
                          Beacon   :: none | string(),
                          Warps    :: [sector()],
                          Stations :: [{Name :: station_name(),
                                        Type :: station_type()}],
                          Planets  :: [{Name  :: planet_name(),
                                        Type  :: planet_type(),
                                        Owner :: owner()}],
                          Squadron :: {Size  :: pos_integer(),
                                       Mode  :: squadron_mode(),
                                       Owner :: owner()},
                          Ships    :: [{Name   :: ship_name(),
                                        Type   :: ship_type(),
                                        Escort :: non_neg_integer(),
                                        Pilot  :: pilot(),
                                        Owner  :: owner()}]}.
-type sector_event()  :: {arrival, ship_name(), ship_type(), pilot(), owner()}
                       | {departure, ship_name(), pilot(), sector()}.
-type resource()      :: fuel | metal | silicate | carbon.
-type station_class() :: 1..8 | shipyard | stardock.


-spec listen(PortNum) -> Result
    when PortNum :: inet:port_num(),
         Result  :: ok
                  | {error, {listening, inet:port_num()}}.
%% @doc
%% Make the server start listening on a port.
%% Returns an {error, Reason} tuple if it is already listening.

listen(PortNum) ->
    sj_client_man:listen(PortNum).


-spec ignore() -> ok.
%% @doc
%% Make the server stop listening if it is, or continue to do nothing if it isn't.

ignore() ->
    sj_client_man:ignore().


-spec bigbang(Magnitude) -> ok | error
    when Magnitude :: pos_integer().
%% @doc
%% Generate a new universe if one does not already exist.

bigbang(Magnitude) ->
    sj_sector_man:bigbang(Magnitude).


-spec bigcrunch() -> ok.

bigcrunch() ->
    ok = sj_client_man:wipe(),
    ok = sj_stats:wipe(),
    ok = sj_alien_man:wipe(),
    ok = sj_ship_man:wipe(),
    ok = sj_planet_man:wipe(),
    ok = sj_squadron_man:wipe(),
    ok = sj_station_man:wipe(),
    ok = sj_sector_man:bigcrunch(),
    tell(info, "The Big Crunch has obliterated the Universe!").


-spec start() -> ok.
%% @doc
%% Start the server in an "ignore" state.

start() ->
    application:start(space_jerks).


-spec stop() -> no_return().

stop() ->
    ok = sj_ship_man:cache(),
    ok = sj_client_man:cache(),
    ok = tell(info, "Caching game data before shutting down..."),
    zx:stop().


-spec start(normal, term()) -> {ok, pid()}.
%% @private
%% Called by OTP to kick things off. This is for the use of the "application" part of
%% OTP, not to be called by user code.
%% See: http://erlang.org/doc/apps/kernel/application.html

start(normal, _Args) ->
    ok = application:ensure_started(sasl),
    Result = sj_sup:start_link(),
    ok = sj_client_man:listen(2222),
    Result.



-spec stop(term()) -> ok.
%% @private
%% Similar to start/2 above, this is to be called by the "application" part of OTP,
%% not client code. Causes a (hopefully graceful) shutdown of the application.

stop(_) ->
    ok.
