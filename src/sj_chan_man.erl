%%% @doc
%%% Space Jerks Chan Manager
%%% @end

-module(sj_chan_man).
-vsn("0.1.0").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Client Interface
-export([lookup/1, list/0, create/2]).
%% gen_server
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {chans = #{} :: #{space_jerks:chan() := pid()},
         mons  = #{} :: #{reference() := space_jerks:chan()}}).


-type state() :: #s{}.



%%% Client Interface

-spec lookup(space_jerks:chan()) -> {ok, pid()} | error.

lookup(Chan) ->
    gen_server:call(?MODULE, {lookup, Chan}).


-spec list() -> [space_jerks:chan()].

list() ->
    gen_server:call(?MODULE, list).


-spec create(space_jerks:chan(), space_jerks:username()) -> ok | error.

create(Chan, User) ->
    gen_server:call(?MODULE, {create, Chan, User, self()}).



%%% gen_server

-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason :: term()}.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {ok, state()}.

init(none) ->
    State = #s{},
    {ok, State}.


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().

handle_call({lookup, Chan}, _, State) ->
    Response = do_lookup(Chan, State),
    {reply, Response, State};
handle_call(list, _, State) ->
    Response = do_list(State),
    {reply, Response, State};
handle_call({create, Chan, User, PID}, _, State) ->
    {Response, NewState} = do_create(Chan, User, PID, State),
    {reply, Response, NewState};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "~p Unexpected call from ~tp: ~tp~n", [self(), From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_cast(Unexpected, State) ->
    ok = log(warning, "~p Unexpected cast: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_info({'DOWN', Mon, process, PID, Reason}, State) ->
    NewState = handle_down(Mon, PID, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "~p Unexpected info: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


handle_down(Mon, PID, Reason, State = #s{chans = Chans, mons = Mons}) ->
    case maps:take(Mon, Mons) of
        {Chan, NewMons} ->
            NewChans = maps:remove(Chan, Chans),
            State#s{chans = NewChans, mons = NewMons};
        error ->
            Unexpected = {'DOWN', Mon, process, PID, Reason},
            ok = log(warning, "Unexpected info: ~tp", [Unexpected]),
            State
    end.


-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().

code_change(_, State, _) ->
    {ok, State}.


-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().

terminate(_, _) ->
    ok.



%%% Doer Functions

do_lookup(Chan, #s{chans = Chans}) ->
    maps:find(Chan, Chans).


do_list(#s{chans = Chans}) ->
    maps:keys(Chans).


do_create(Chan, User, PID, State = #s{chans = Chans, mons = Mons}) ->
    case maps:is_key(Chan, Chans) of
        false ->
            {ok, ChanPID} = supervisor:start_child(sj_chan_sup, [Chan, User, PID]),
            Mon = monitor(process, ChanPID),
            NewChans = maps:put(Chan, ChanPID, Chans),
            NewMons = maps:put(Mon, Chan, Mons),
            {ok, State#s{chans = NewChans, mons = NewMons}};
        true ->
            {error, State}
    end.
