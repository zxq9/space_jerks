%%% @doc
%%% Space Jerks Sector Supervisor
%%% @end

-module(sj_sector_sup).
-vsn("0.1.0").
-behaviour(supervisor).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").


-export([create_sector/1]).
-export([start_link/0]).
-export([init/1]).



-spec create_sector(ID) -> Result
    when ID     :: space_jerks:sector_id(),
         Result :: {ok, pid()}
                 | {error, Reason},
         Reason :: {already_started, pid()}
                 | {shutdown, term()}
                 | term().

create_sector(ID) ->
    supervisor:start_child(?MODULE, [ID]).


-spec start_link() -> {ok, pid()}.
%% @private
%% This supervisor's own start function.

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, none).


-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
%% @private
%% The OTP init/1 function.

init(none) ->
    RestartStrategy = {simple_one_for_one, 1, 60},
    Sector = {sj_sector,
              {sj_sector, start_link, []},
              temporary,
              brutal_kill,
              worker,
              [sj_sector]},
    {ok, {RestartStrategy, [Sector]}}.
