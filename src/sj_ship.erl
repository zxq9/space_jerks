%%% @doc
%%% Space Jerks Ship
%%% @end

-module(sj_ship).
-vsn("0.1.0").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Pilot Interface
-export([view/1, warp/2, move/2, radio/3,
         claim/2, embark/2, disembark/1, abandon/1,
         owner/1]).
%% Environment
-export([sector_event/2]).
%% Management
-export([status/1]).
%% gen_server
-export([start_link/1, start_link/4]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).

-opaque([info/0]).

-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {id             = 1              :: space_jerks:ship_id(),
         type           = ""             :: space_jerks:ship_type(),
         make           = ""             :: space_jerks:ship_make(),
         name           = ""             :: space_jerks:ship_name(),
         pilot          = none           :: none | space_jerks:pilot(),
         pilot_mon      = none           :: none | reference(),
         owner          = none           :: space_jerks:owner(),
         loc            = none           :: none | space_jerks:sector(),
         loc_pid        = none           :: none | pid(),
         loc_mon        = none           :: none | reference(),
         heat           = 0              :: integer(),
         sink           = 100            :: pos_integer(),
         efficiency     = 1              :: pos_integer(),
         cooling        = 1              :: pos_integer(),
         scanner        = none           :: none | scanner(),
         tw_drive       = none           :: none | pos_integer(),
         tp_range       = 10             :: pos_integer(),
         atmospheric    = true           :: boolean(),
         escape_pod     = true           :: boolean(),
         holds          = 35             :: non_neg_integer(),
         metal          = 0              :: non_neg_integer(),
         silica         = 0              :: non_neg_integer(),
         carbon         = 0              :: non_neg_integer(),
         fighters       = 200            :: non_neg_integer(),
         shields        = 100            :: non_neg_integer(),
         armor          = 200            :: non_neg_integer(),
         probes         = 0              :: non_neg_integer(),
         beacons        = 0              :: non_neg_integer(),
         disruptors     = 0              :: non_neg_integer(),
         genesis_t      = 0              :: non_neg_integer(),
         ported         = 0              :: non_neg_integer(),
         landed         = 0              :: non_neg_integer(),
         kills          = 0              :: non_neg_integer(),
         created        = os:timestamp() :: erlang:timestamp(),
         ts             = os:timestamp() :: erlang:timestamp()}).


-type state()   :: #s{}.
-type info()    :: state().
-type scanner() :: density | holo.



%%% Pilot Interface

-spec view(Ship) -> ok
    when Ship :: pid().

view(Ship) ->
    gen_server:cast(Ship, view).


-spec move(Ship, Sector) -> ok | error
    when Ship   :: pid(),
         Sector :: space_jerks:sector().

move(Ship, Sector) ->
    gen_server:call(Ship, {move, Sector}).


-spec warp(Ship, Sector) -> ok
    when Ship   :: pid(),
         Sector :: pid().

warp(Ship, Sector) ->
    gen_server:cast(Ship, {warp, Sector}).


-spec radio(Ship, Range, Message) -> ok
    when Ship    :: pid(),
         Range   :: 1..9,
         Message :: string().

radio(Ship, Range, Message) ->
    gen_server:cast(Ship, {radio, Range, Message}).


-spec claim(Ship, Owner) -> ok
    when Ship  :: pid(),
         Owner :: space_jerks:owner().

claim(Ship, Owner) ->
    gen_server:cast(Ship, {claim, Owner}).


-spec embark(Ship, Pilot) -> ok
    when Ship  :: pid(),
         Pilot :: space_jerks:pilot().

embark(Ship, Pilot) ->
    gen_server:cast(Ship, {embark, Pilot}).


-spec disembark(Ship) -> ok
    when Ship :: pid().

disembark(Ship) ->
    gen_server:cast(Ship, disembark).


-spec abandon(Ship) -> ok
    when Ship :: pid().

abandon(Ship) ->
    gen_server:cast(Ship, abandon).


-spec owner(pid()) -> space_jerks:owner().

owner(Ship) ->
    gen_server:call(Ship, owner).



%%% Environment

-spec sector_event(Ship, Event) -> ok
    when Ship  :: pid(),
         Event :: space_jerks:sector_event().

sector_event(Ship, Event) ->
    gen_server:cast(Ship, {sector_event, Event}).


%%% Management

-spec status(Ship) -> SanitizedState
    when Ship           :: pid(),
         SanitizedState :: state().

status(Ship) ->
    gen_server:call(Ship, status).


%%% gen_server

-spec start_link(ID, Type, Make, Name) -> Result
    when ID     :: space_jerks:ship_id(),
         Type   :: space_jerks:ship_type(),
         Make   :: space_jerks:ship_make(),
         Name   :: space_jerks:ship_name(),
         Result :: {ok, pid()}
                 | {error, Reason :: term()}.

start_link(ID, Type, Make, Name) ->
    gen_server:start_link(?MODULE, {ID, Type, Make, Name}, []).


-spec start_link(Info) -> Result
    when Info   :: info(),
         Result :: {ok, pid()}
                 | {error, Reason :: term()}.

start_link(Info) ->
    gen_server:start_link(?MODULE, Info, []).


-spec init({ID, Info}) -> {ok, state()}
    when ID   :: space_jerks:ship_id(),
         Info :: info().


init(Info = #s{id = ID, loc = LocID}) ->
    {ok, LocPID} = sj_sector_man:lookup(LocID),
    State = make_jump(LocPID, Info#s{pilot = none, pilot_mon = none}),
    ok = sj_ship_man:launch(ID, State),
    {ok, State};
init({ID, Type, Make, Name}) ->
    State = #s{id = ID, type = Type, make = Make, name = Name},
    {ok, State}.


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().

handle_call({move, Sector}, _, State) ->
    {Response, NewState} = do_move(Sector, State),
    {reply, Response, NewState};
handle_call(owner, _, State = #s{owner = Owner}) ->
    {reply, Owner, State};
handle_call(status, _, State) ->
    Response = do_status(State),
    {reply, Response, State};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "~p Unexpected call from ~tp: ~tp~n", [self(), From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_cast(view, State) ->
    ok = do_view(State),
    {noreply, State};
handle_cast({warp, Sector}, State) ->
    NewState = do_warp(Sector, State),
    {noreply, NewState};
handle_cast({sector_event, Event}, State) ->
    ok = do_sector_event(Event, State),
    {noreply, State};
handle_cast({radio, Range, Message}, State) ->
    ok = do_radio(Range, Message, State),
    {noreply, State};
handle_cast({claim, Owner}, State) ->
    NewState = do_claim(Owner, State),
    {noreply, NewState};
handle_cast({embark, Pilot}, State) ->
    NewState = do_embark(Pilot, State),
    {noreply, NewState};
handle_cast(disembark, State) ->
    NewState = do_disembark(State),
    {noreply, NewState};
handle_cast(abandon, State) ->
    NewState = do_abandon(State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "~p Unexpected cast: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_info({'DOWN', Mon, process, PID, Reason}, State) ->
    NewState = handle_down(Mon, PID, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "~p Unexpected info: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


handle_down(Mon, PID, _, State = #s{pilot_mon = Mon, pilot = {Type, Name, PID}}) ->
    NewState = State#s{pilot = {Type, Name, none}, pilot_mon = none},
    ok = update_ship(NewState),
    NewState;
handle_down(Mon, PID, _, State = #s{loc_mon = Mon, loc_pid = PID}) ->
    ok = sj_ship_man:bounce(),
    State#s{loc = none, loc_pid = none, loc_mon = none};
handle_down(Mon, PID, Reason, State) ->
    Unexpected = {'DOWN', Mon, process, PID, Reason},
    ok = log(warning, "Unexpected info: ~tp", [Unexpected]),
    State.



%%% OTP Service Functions

-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().

code_change(_, State, _) ->
    {ok, State}.


-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().

terminate(_, _) ->
    ok.



%%% Doer Functions

do_view(#s{loc_pid = none}) ->
    ok;
do_view(#s{loc_pid = LocPID, pilot = Pilot}) ->
    View = sj_sector:view(LocPID),
    show(Pilot, View).


do_move(_, State = #s{loc_pid = none}) ->
    {error, State};
do_move(Sector, State = #s{loc_pid = LocPID}) ->
    case sj_sector:sector_lookup(LocPID, Sector) of
        {ok, PID} -> {ok, do_warp(PID, State)};
        error     -> {error, State}
    end.


do_warp(Sector, State = #s{loc_pid = none}) ->
    make_jump(Sector, State);
do_warp(Sector, State = #s{id = ID, loc_pid = PID, loc_mon = Mon}) ->
    true = demonitor(Mon),
    ok = sj_sector:depart(PID, ID),
    make_jump(Sector, State).

make_jump(Sector,
          State = #s{id = ID, name = Name, type = Type, make = Make,
                     fighters = Fighters, pilot = Pilot, owner = Owner}) ->
    {ok, View} = sj_sector:arrive(Sector, ID, Name, Type, Make, Fighters, Pilot, Owner),
    ok = show(Pilot, View),
    SectorID = element(1, View),
    Mon = monitor(process, Sector),
    State#s{loc = SectorID, loc_pid = Sector, loc_mon = Mon}.


show({user, _, PID}, View) when is_pid(PID) ->
    sj_client:show_sector(PID, View);
show({alien, _, PID}, View) when is_pid(PID) ->
    sj_alien:show_sector(PID, View);
show(_, _) ->
    ok.


do_sector_event(Event, #s{pilot = {user, _, PID}}) when is_pid(PID) ->
    sj_client:show_action(PID, Event);
do_sector_event(_, _) ->
    ok.


do_radio(Range, Message, #s{loc = LocID, loc_pid = LocPID, pilot = {_, Name, _}})
        when is_pid(LocPID) ->
    sj_sector:radio(LocPID, LocID, Name, Range, Message);
do_radio(_, _, _) ->
    ok.


do_claim(Owner, State) ->
    NewState = State#s{owner = Owner},
    ok = update_ship(NewState),
    NewState.


do_embark(Pilot = {Type, _, PID}, State = #s{loc_pid = Sector}) ->
    Mon = monitor(process, PID),
    ok =
        case Type =:= user andalso is_pid(Sector) of
            true  -> show(Pilot, sj_sector:view(Sector));
            false -> ok
        end,
    NewState = State#s{pilot = Pilot, pilot_mon = Mon},
    ok = update_ship(NewState),
    NewState.
    


do_disembark(State) ->
    NewState = State#s{pilot = none},
    ok = update_ship(NewState),
    NewState.


do_abandon(State) ->
    NewState = State#s{owner = none, pilot = none},
    ok = update_ship(NewState),
    NewState.


update_ship(#s{id = ID, name = Name, fighters = Escort, pilot = Driver, owner = Owner,
               loc_pid = Sector}) ->
    ok = sj_ship_man:cache(),
    Pilot =
        case is_tuple(Driver) of
            true  -> erlang:delete_element(3, Driver);
            false -> Driver
        end,
    sj_sector:update_ship(Sector, ID, Name, Escort, Pilot, Owner).


do_status(State) ->
    State#s{pilot = none, pilot_mon = none, loc_pid = none, loc_mon = none}.
