%%% @doc
%%% Space Jerks Stats and Scores
%%% @end

-module(sj_stats).
-vsn("0.1.0").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Client Interface
-export([record/3, scores/0]).
%% Service Interface
-export([cache/0, cache_path/0, wipe/0]).
%% gen_server
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {scores = #{} :: scores()}).


-type state()  :: #s{}.
-type score()  :: {XP :: non_neg_integer(), Alignment :: integer()}.
-type scores() :: #{space_jerks:username() := score()}.



%%% Client Interface

-spec record(User, XP, Alignment) -> ok
    when User      :: space_jerks:username(),
         XP        :: non_neg_integer(),
         Alignment :: integer().

record(User, XP, Alignment) ->
    gen_server:cast(?MODULE, {record, User, XP, Alignment}).


-spec scores() -> scores().

scores() ->
    gen_server:call(?MODULE, scores).



%%% Service Interface


-spec cache() -> ok.

cache() ->
    gen_server:call(?MODULE, cache).


-spec cache_path() -> file:filename().

cache_path() ->
    filename:join(zx_lib:path(var, "otpr", "space_jerks"), "stats.erlbin").


-spec wipe() -> ok.

wipe() ->
    gen_server:call(?MODULE, wipe).



%%% gen_server

-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason :: term()}.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {ok, state()}.

init(none) ->
    Path = cache_path(),
    ok = filelib:ensure_dir(Path),
    {1, Scores} =
        case file:read_file(Path) of
            {ok, Bin}       -> binary_to_term(Bin);
            {error, enoent} -> {1, #{}}
        end,
    State = #s{scores = Scores},
    {ok, State}.


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().

handle_call(scores, _, State = #s{scores = Scores}) ->
    {reply, Scores, State};
handle_call(cache, _, State) ->
    ok = do_cache(State),
    {reply, ok, State};
handle_call(wipe, _, State) ->
    NewState = do_wipe(State),
    {reply, ok, NewState};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "~p Unexpected call from ~tp: ~tp~n", [self(), From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_cast({record, User, XP, Alignment}, State = #s{scores = Scores}) ->
    {noreply, State#s{scores = maps:put(User, {XP, Alignment}, Scores)}};
handle_cast(Unexpected, State) ->
    ok = log(warning, "~p Unexpected cast: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().

handle_info(Unexpected, State) ->
    ok = log(warning, "~p Unexpected info: ~tp~n", [self(), Unexpected]),
    {noreply, State}.



%%% OTP Service Functions

-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().

code_change(_, State, _) ->
    {ok, State}.


-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().

terminate(Reason, State) ->
    ok = log(info, "Terminating with ~p", [Reason]),
    do_cache(State).


do_cache(#s{scores = Scores}) ->
    Path = cache_path(),
    Data = term_to_binary({1, Scores}, [{compressed, 9}]),
    file:write_file(Path, Data).


do_wipe(_) ->
    Path = cache_path(),
    ok =
        case filelib:is_file(Path) of
            true  -> zx_lib:rm(Path);
            false -> ok
        end,
    #s{}.
