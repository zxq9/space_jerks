%%% @doc
%%% Space Jerks Client Manager
%%% @end

-module(sj_client_man).
-vsn("0.1.0").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Network Service Interface
-export([listen/1, ignore/0]).
%% Client Handler Interface
-export([enroll/0, lookup/1, load_user/1, login/1, create_user/2]).
%% Service Interface
-export([cache/0, cache_path/0, wipe/0]).
%% gen_server
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {port_num = none :: none | inet:port_number(),
         listener = none :: none | gen_tcp:socket(),
         clients  = []   :: [{pid(), none | space_jerks:username()}],
         users    = #{}  :: #{space_jerks:username() := user()}}).

-record(user,
        {passdata  = none :: none | pass_data(),
         pid       = none :: none | pid()}).


-type state()     :: #s{}.
-type user()      :: #user{}.
-type pass_data() :: {Type :: crypto:hash_algotirhm(),
                      Salt :: binary(),
                      Hash :: binary()}.



%%% Network Service Interface


-spec listen(PortNum) -> Result
    when PortNum :: inet:port_number(),
         Result  :: ok
                  | {error, Reason},
         Reason  :: {listening, inet:port_number()}.
%% @doc
%% Tell the service to start listening on a given port.
%% Only one port can be listened on at a time in the current implementation, so
%% an error is returned if the service is already listening.

listen(PortNum) ->
    gen_server:call(?MODULE, {listen, PortNum}).


-spec ignore() -> ok.
%% @doc
%% Tell the service to stop listening.
%% It is not an error to call this function when the service is not listening.

ignore() ->
    gen_server:cast(?MODULE, ignore).



%%% Client Handler Interface


-spec enroll() -> ok.
%% @doc
%% Clients register here when they establish a connection.
%% Other processes can enroll as well.

enroll() ->
    gen_server:cast(?MODULE, {enroll, self()}).


-spec lookup(Name) -> {ok, pid()} | error
    when Name :: space_jerks:username().

lookup(Name) ->
    gen_server:call(?MODULE, {lookup, Name}).


load_user(Name) ->
    gen_server:call(?MODULE, {load_user, Name}).


login(Name) ->
    gen_server:cast(?MODULE, {login, Name, self()}).


create_user(Name, PassData) ->
    gen_server:call(?MODULE, {create_user, Name, PassData}).



%%% Service Interface

-spec cache() -> ok.

cache() ->
    gen_server:call(?MODULE, cache).


-spec cache_path() -> file:filename().

cache_path() ->
    filename:join(zx_lib:path(var, "otpr", "space_jerks"), "users.erlbin").


-spec wipe() -> ok.

wipe() ->
    gen_server:call(?MODULE, wipe).



%%% gen_server


-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason :: term()}.
%% @private
%% This should only ever be called by sj_clients (the service-level supervisor).

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {ok, state()}.
%% @private
%% Called by the supervisor process to give the process a chance to perform any
%% preparatory work necessary for proper function.

init(none) ->
    Path = cache_path(),
    {1, Users} =
        case file:read_file(Path) of
            {ok, Bin}       -> binary_to_term(Bin);
            {error, enoent} -> {1, #{}}
        end,
    State = #s{users = Users},
    {ok, State}.


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().
%% @private
%% The gen_server:handle_call/3 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_call-3

handle_call({listen, PortNum}, _, State) ->
    {Response, NewState} = do_listen(PortNum, State),
    {reply, Response, NewState};
handle_call({lookup, Name}, _, State) ->
    Response = do_lookup(Name, State),
    {reply, Response, State};
handle_call({load_user, Name}, _, State) ->
    Response = do_load_user(Name, State),
    {reply, Response, State};
handle_call({create_user, Name, PassData}, _, State) ->
    {Response, NewState} = do_create_user(Name, PassData, State),
    {reply, Response, NewState};
handle_call(cache, _, State) ->
    ok = do_cache(State),
    {reply, ok, State};
handle_call(wipe, _, State) ->
    NewState = do_wipe(State),
    {reply, ok, NewState};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "~p Unexpected call from ~tp: ~tp~n", [self(), From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_cast/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_cast-2

handle_cast({enroll, PID}, State) ->
    NewState = do_enroll(PID, State),
    {noreply, NewState};
handle_cast({login, Name, PID}, State) ->
    NewState = do_login(Name, PID, State),
    {noreply, NewState};
handle_cast(ignore, State) ->
    NewState = do_ignore(State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "~p Unexpected cast: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_info/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_info({'DOWN', Mon, process, PID, Reason}, State) ->
    NewState = handle_down(Mon, PID, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "~p Unexpected info: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


-spec handle_down(Mon, PID, Reason, State) -> NewState
    when Mon      :: reference(),
         PID      :: pid(),
         Reason   :: term(),
         State    :: state(),
         NewState :: state().

handle_down(Mon, PID, Reason, State = #s{clients = Clients, users = Users}) ->
    case lists:keytake(PID, 1, Clients) of
        {value, {PID, none}, NewClients} ->
            State#s{clients = NewClients};
        {value, {PID, Name}, NewClients} ->
            Selected = maps:get(Name, Users),
            NewUsers = maps:put(Name, Selected#user{pid = none}, Users),
            State#s{clients = NewClients, users = NewUsers};
        false ->
            Unexpected = {'DOWN', Mon, process, PID, Reason},
            ok = log(warning, "Unexpected info: ~tp", [Unexpected]),
            State
    end.



%%% OTP Service Functions

-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().
%% @private
%% The gen_server:code_change/3 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:code_change-3

code_change(_, State, _) ->
    {ok, State}.


-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().
%% @private
%% The gen_server:terminate/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:terminate-2

terminate(_, _) ->
    ok.


%%% Doer Functions

-spec do_listen(PortNum, State) -> {Result, NewState}
    when PortNum  :: inet:port_number(),
         State    :: state(),
         Result   :: ok
                   | {error, Reason :: {listening, inet:port_number()}},
         NewState :: state().
%% @private
%% The "doer" procedure called when a "listen" message is received.

do_listen(PortNum, State = #s{listener = none}) ->
    SocketOptions =
        [inet6,
         {packet,    line},
         {active,    once},
         {mode,      list},
         {keepalive, true},
         {reuseaddr, true}],
    {ok, Listener} = gen_tcp:listen(PortNum, SocketOptions),
    {ok, _} = sj_client:start(Listener),
    {ok, State#s{port_num = PortNum, listener = Listener}};
do_listen(_, State = #s{port_num = PortNum}) ->
    ok = tell(info, "Already listening on ~p~n", [PortNum]),
    {{error, {listening, PortNum}}, State}.


-spec do_ignore(State) -> NewState
    when State    :: state(),
         NewState :: state().
%% @private
%% The "doer" procedure called when an "ignore" message is received.

do_ignore(State = #s{listener = none}) ->
    State;
do_ignore(State = #s{listener = Listener, clients = Clients}) ->
    ok = gen_tcp:close(Listener),
    NewClients = lists:keydelete(none, 2, Clients),
    State#s{listener = none, port_num = none, clients = NewClients}.


-spec do_enroll(PID, State) -> NewState
    when PID      :: pid(),
         State    :: state(),
         NewState :: state().

do_enroll(PID, State = #s{clients = Clients}) ->
    case lists:keymember(PID, 1, Clients) of
        false ->
            _ = monitor(process, PID),
            State#s{clients = [{PID, none} | Clients]};
        true ->
            State
    end.


do_login(Name, PID, State = #s{users = Users}) ->
    case maps:find(Name, Users) of
        {ok, This} -> State#s{users = maps:put(Name, This#user{pid = PID}, Users)};
        error      -> State
    end.


do_lookup(Name, #s{users = Users}) ->
    case maps:find(Name, Users) of
        {ok, #user{pid = none}} -> error;
        {ok, #user{pid = PID}}  -> {ok, PID};
        error                   -> error
    end.


do_load_user(String, #s{users = Users}) ->
    case maps:find(String, Users) of
        {ok, #user{passdata = PassData}} -> {ok, PassData};
        error                            -> error
    end.


do_create_user(Name, PassData, State = #s{users = Users}) ->
    case maps:is_key(Name, Users) of
        false ->
            NewUsers = maps:put(Name, #user{passdata = PassData}, Users),
            NewState = State#s{users = NewUsers},
            ok = do_cache(NewState),
            {ok, NewState};
        true ->
            {error, State}
    end.


do_cache(#s{users = Users}) ->
    ok = tell(info, "Caching client data."),
    Path = cache_path(),
    Data = term_to_binary({1, drop_pids(Users)}, [{compressed, 9}]),
    file:write_file(Path, Data).

drop_pids(Users) ->
    Iterator = maps:iterator(Users),
    drop_pids(Iterator, Users).

drop_pids(none, Users) ->
    Users;
drop_pids(Iterator, Users) ->
    {K, V, Next} = maps:next(Iterator),
    drop_pids(Next, maps:put(K, V#user{pid = none}, Users)).


do_wipe(State) ->
    #s{clients = Clients} = do_ignore(State),
    Kill = fun({PID, _}) -> supervisor:terminate_child(sj_client_sup, PID) end,
    ok = lists:foreach(Kill, Clients),
    Path = cache_path(),
    ok =
        case filelib:is_file(Path) of
            true  -> zx_lib:rm(Path);
            false -> ok
        end,
    UserFiles = filelib:wildcard(filename:join(filename:dirname(Path), "user-*")),
    ok = lists:foreach(fun file:delete/1, UserFiles),
    #s{}.
