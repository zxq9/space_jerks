%%% @doc
%%% Space Jerks Channel Supervisor
%%% @end

-module(sj_chan_sup).
-vsn("0.1.0").
-behaviour(supervisor).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").


-export([start_link/0]).
-export([init/1]).



-spec start_link() -> {ok, pid()}.

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, none).


-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.

init(none) ->
    RestartStrategy = {simple_one_for_one, 1, 60},
    Chan = {sj_chan,
            {sj_chan, start_link, []},
            temporary,
            brutal_kill,
            worker,
            [sj_chan]},
    {ok, {RestartStrategy, [Chan]}}.
