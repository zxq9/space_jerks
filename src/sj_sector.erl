%%% @doc
%%% Space Jerks Sector
%%%
%%% This is where sectors are defined. A graph of sector relationships define the
%%% universe map.
%%% @end

-module(sj_sector).
-vsn("0.1.0").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Mob Interface
-export([view/1, radio/5, arrive/8, depart/2, bounce_in/2,
         update_ship/6,
         sector_lookup/2]).
%% Manager Interface
-export([peer/2]).
%% Startup
-export([start_link/1]).
%% gen_server
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {id       = 1    :: space_jerks:sector(),
         name     = none :: none | string(),
         cluster  = none :: none | string(),
         beacon   = none :: none | string(),
         mons     = #{}  :: #{reference() := mon_type()},
         warps    = []   :: [warp()],
         station  = none :: station(),
         planets  = []   :: [planet()],
         squadron = none :: none | squadron(),
         ships    = []   :: [ship()]}).

-record(warp,
        {id  = 1    :: space_jerks:sector(),
         pid = none :: none | pid(),
         mon = none :: none | reference()}).

-record(station,
        {id   = 1    :: space_jerks:station_id(),
         name = ""   :: space_jerks:station_name(),
         type = 1    :: space_jerks:station_type(),
         pid  = none :: none | pid(),
         mon  = none :: none | reference()}).

-record(planet,
        {id    = 1    :: space_jerks:planet_id(),
         name  = ""   :: space_jerks:planet_name(),
         type  = none :: none | space_jerks:planet_type(),
         owner = none :: none | space_jerks:owner(),
         pid   = none :: none | pid(),
         mon   = none :: none | reference()}).

-record(squadron,
        {id    = 0         :: space_jerks:squadron_id(),
         owner = rogue     :: space_jerks:owner(),
         mode  = defensive :: none | space_jerks:squadron_mode(),
         size  = 1         :: pos_integer(),
         pid   = none      :: none | pid(),
         mon   = none      :: none | reference()}).

-record(ship,
        {id     = 0         :: space_jerks:ship_id(),
         name   = ""        :: space_jerks:ship_name(),
         type   = freighter :: space_jerks:ship_type(),
         make   = ""        :: space_jerks:ship_make(),
         escort = 0         :: non_neg_integer(),
         pilot  = none      :: none | space_jerks:pilot(),
         owner  = none      :: space_jerks:owner(),
         pid    = none      :: none | pid(),
         mon    = none      :: none | reference()}).

-type state()    :: #s{}.
-type mon_type() :: warp | station | planet | ship.
-type warp()     :: #warp{}.
-type station()  :: #station{}.
-type planet()   :: #planet{}.
-type squadron() :: #squadron{}.
-type ship()     :: #ship{}.



%%% Mob Interface

-spec view(Sector) -> View
    when Sector :: pid(),
         View   :: space_jerks:sector_view().

view(Sector) ->
    gen_server:call(Sector, view).


-spec radio(Sector, OriginLoc, Sender, Intensity, Message) -> ok
    when Sector    :: pid(),
         OriginLoc :: space_jerks:sector(),
         Sender    :: space_jerks:username(),
         Intensity :: pos_integer(),
         Message   :: string().

radio(Sector, OriginLoc, Sender, Intensity, Message) ->
    gen_server:cast(Sector, {radio, OriginLoc, Sender, Intensity, Message}).


-spec arrive(Sector, ID, Name, Type, Make, Escort, Pilot, Owner) -> {ok, View} | bounce
    when Sector :: pid(),
         ID     :: space_jerks:ship_id(),
         Name   :: space_jerks:ship_name(),
         Type   :: space_jerks:ship_type(),
         Make   :: space_jerks:ship_make(),
         Escort :: non_neg_integer(),
         Pilot  :: space_jerks:pilot(),
         Owner  :: space_jerks:owner(),
         View   :: space_jerks:sector_view().

arrive(Sector, ID, Name, Type, Make, Escort, Pilot, Owner) ->
    ShipInfo = {self(), ID, Name, Type, Make, Escort, Pilot, Owner},
    gen_server:call(Sector, {arrive, ShipInfo}).



-spec update_ship(Sector, ID, Name, Escort, Pilot, Owner) -> ok
    when Sector :: pid(),
         ID     :: space_jerks:ship_id(),
         Name   :: space_jerks:ship_name(),
         Escort :: non_neg_integer(),
         Pilot  :: space_jerks:pilot(),
         Owner  :: space_jerks:owner().

update_ship(Sector, ID, Name, Escort, Pilot, Owner) ->
    Info = {ID, Name, Escort, Pilot, Owner},
    gen_server:cast(Sector, {update_ship, Info}).


-spec depart(Sector, ID) -> ok
    when Sector :: pid(),
         ID     :: space_jerks:ship_id().

depart(Sector, ID) ->
    gen_server:cast(Sector, {depart, ID}).


-spec bounce_in(Sector, Mob) -> ok
    when Sector :: pid(),
         Mob    :: ship() | station() | planet() | squadron().

bounce_in(Sector, Mob) ->
    gen_server:cast(Sector, {bounce_in, Mob}).


-spec sector_lookup(Sector, Adjacent) -> {ok, pid()} | error
    when Sector   :: pid(),
         Adjacent :: space_jerks:sector().

sector_lookup(Sector, Adjacent) ->
    gen_server:call(Sector, {sector_lookup, Adjacent}).



%%% Manager Interface

-spec peer(Sector, Warps) -> ok
    when Sector :: pid(),
         Warps  :: [{space_jerks:sector(), pid()}].

peer(Sector, Warps) ->
    gen_server:cast(Sector, {peer, Warps}).


%%% Startup Functions


-spec start_link(ID) -> Result
    when ID     :: space_jerks:sector(),
         Result :: {ok, pid()}
                 | {error, Reason :: term()}.
%% @private
%% This should only ever be called by sj_clients (the service-level supervisor).

start_link(ID) ->
    gen_server:start_link(?MODULE, ID, []).


-spec init(ID :: space_jerks:sector()) -> {ok, state()}.
%% @private
%% Called by the supervisor process to give the process a chance to perform any
%% preparatory work necessary for proper function.

init(ID) ->
    State = #s{id = ID},
    {ok, State}.



%%% gen_server Message Handling Callbacks


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().
%% @private
%% The gen_server:handle_call/3 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_call-3

handle_call(view, _, State) ->
    Response = do_view(State),
    {reply, Response, State};
handle_call({sector_lookup, Adjacent}, _, State) ->
    Response = do_sector_lookup(Adjacent, State),
    {reply, Response, State};
handle_call({arrive, ShipInfo}, _, State) ->
    {Response, NewState} = do_arrive(ShipInfo, State),
    {reply, Response, NewState};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "~p Unexpected call from ~tp: ~tp~n", [self(), From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_cast/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_cast-2

handle_cast({radio, OriginLoc, Sender, Intensity, Message}, State) ->
    ok = do_radio(OriginLoc, Sender, Intensity, Message, State),
    {noreply, State};
handle_cast({update_ship, Info}, State) ->
    NewState = do_update_ship(Info, State),
    {noreply, NewState};
handle_cast({depart, ID}, State) ->
    NewState = do_depart(ID, State),
    {noreply, NewState};
handle_cast({peer, Warps}, State) ->
    NewState = do_peer(Warps, State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "~p Unexpected cast: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_info/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_info({'DOWN', Mon, process, PID, Reason}, State) ->
    NewState = handle_down(Mon, PID, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "~p Unexpected info: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


handle_down(Mon, PID, Reason,
            State = #s{station = #station{id = ID, name = Name, pid = PID, mon = Mon},
                       ships = Ships}) ->
    ok = log(info, "Station ~p (~p, ~p) went down with ~p", [Name, ID, PID, Reason]),
    Event = {bloip, station, Name},
    Notify = fun(#ship{pid = P}) -> sj_ship:sector_event(P, Event) end,
    ok = lists:foreach(Notify, Ships),
    State#s{station = none};
handle_down(Mon, PID, Reason, State = #s{mons = Mons}) ->
    case maps:take(Mon, Mons) of
        {Type, NewMons} ->
            scrub(Type, PID, State#s{mons = NewMons});
        error ->
            Unexpected = {'DOWN', Mon, process, PID, Reason},
            ok = log(warning, "Unexpected info: ~tp", [Unexpected]),
            State
    end.


scrub(ship,   PID, State = #s{ships   = Ships}) ->
    State#s{ships   = lists:keydelete(PID,   #ship.pid, Ships)};
scrub(planet, PID, State = #s{planets = Planets}) ->
    State#s{planets = lists:keydelete(PID, #planet.pid, Planets)};
scrub(warp,   PID, State = #s{warps   = Warps}) ->
    State#s{warps   = lists:keydelete(PID,   #warp.pid, Warps)}.



%%% OTP Service Functions

-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().
%% @private
%% The gen_server:code_change/3 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:code_change-3

code_change(_, State, _) ->
    {ok, State}.


-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().
%% @private
%% The gen_server:terminate/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:terminate-2

terminate(_, _) ->
    ok.



%%% Doer Functions

do_view(#s{id = ID, name = Name, cluster = Cluster, beacon = Beacon,
           warps = Warps, station = Station, squadron = Squad, ships = Ships}) ->
    Squadron =
        case Squad of
            none                                               -> none;
            #squadron{size = Size, mode = Mode, owner = Owner} -> {Size, Mode, Owner}
        end,
    Stations =
        case Station of
            #station{name = SN, type = ST} -> [{SN, ST}];
            none                           -> []
        end,
    {ID,
     Name,
     Cluster,
     Beacon,
     [WID || #warp{id = WID} <- Warps],
     Stations,
     Squadron,
     [{N, T, E, P, O}
        || #ship{name = N, type = T, escort = E, pilot = P, owner = O} <- Ships]}.


do_sector_lookup(Adjacent, #s{warps = Warps}) ->
    case lists:keyfind(Adjacent, #warp.id, Warps) of
        #warp{pid = PID} -> {ok, PID};
        false            -> error
    end.


do_radio(OriginLoc, Sender, 1, Message, #s{ships = Ships}) ->
    usercast(OriginLoc, Sender, Message, Ships);
do_radio(OriginLoc, Sender, Intensity, Message,
         #s{id = ID, warps = Warps, ships = Ships}) ->
    WaveFront = lists:keydelete(OriginLoc, #warp.id, Warps),
    ok = propagate(ID, Sender, Intensity - 1, Message, WaveFront),
    usercast(OriginLoc, Sender, Message, Ships).


usercast(OriginLoc, Sender, Message, [#ship{pilot = {user, _, PID}} | Rest])
        when is_pid(PID) ->
    Payload = {OriginLoc, Sender, Message},
    ok = sj_client:show_message(PID, Payload),
    usercast(OriginLoc, Sender, Message, Rest);
usercast(OriginLoc, Sender, Message, [_ | Rest]) ->
    usercast(OriginLoc, Sender, Message, Rest);
usercast(_, _, _, []) ->
    ok.

propagate(OriginLoc, Sender, Intensity, Message, WaveFront) ->
    Propagate =
        fun(#warp{pid = PID}) ->
            sj_sector:radio(PID, OriginLoc, Sender, Intensity, Message)
        end,
    lists:foreach(Propagate, WaveFront).


do_arrive({PID, ID, Name, Type, Make, Escort, Pilot, Owner},
          State = #s{mons = Mons, ships = Ships}) ->
    Mon = monitor(process, PID),
    New = #ship{id    = ID,    name  = Name,
                type  = Type,  make  = Make, escort = Escort,
                pilot = Pilot, owner = Owner,
                pid   = PID,   mon   = Mon},
    NewMons = maps:put(Mon, ship, Mons),
    ok = sector_event({arrival, Name, Pilot, Owner}, Ships),
    NewShips = [New | Ships],
    NewState = State#s{mons = NewMons, ships = NewShips},
    {{ok, do_view(State)}, NewState}.


sector_event(Event, Ships) ->
    Propagate = fun(#ship{pid = PID}) -> sj_ship:sector_event(PID, Event) end,
    lists:foreach(Propagate, Ships).


do_update_ship({ID, Name, Escort, Pilot, Owner}, State = #s{ships = Ships}) ->
    case lists:keyfind(ID, #ship.id, Ships) of
        Selected = #ship{} ->
            Updated = Selected#ship{name   = Name,
                                    escort = Escort,
                                    pilot  = Pilot,
                                    owner  = Owner},
            NewShips = lists:keystore(ID, #ship.id, Ships, Updated),
            State#s{ships = NewShips};
        false ->
            State
    end.


do_depart(ID, State = #s{mons = Mons, ships = Ships}) ->
    case lists:keytake(ID, #ship.id, Ships) of
        {value, #ship{name = Name, pilot = Pilot, mon = Mon}, NewShips} ->
            true = demonitor(Mon),
            NewMons = maps:remove(Mon, Mons),
            ok = sector_event({departure, Name, Pilot}, NewShips),
            State#s{mons = NewMons, ships = NewShips};
        false ->
            State
    end.


do_peer([{ID, PID} | Rest], State = #s{mons = Mons, warps = Warps}) ->
    {NewMons, NewWarps} =
        case lists:keymember(PID, #warp.pid, Warps) of
            false ->
                Mon = monitor(process, PID),
                NextMons = maps:put(Mon, warp, Mons),
                NewWarp = #warp{id = ID, pid = PID, mon = Mon},
                NextWarps = lists:keystore(ID, #warp.id, Warps, NewWarp),
                {NextMons, NextWarps};
            true ->
                {Mons, Warps}
        end,
    do_peer(Rest, State#s{mons = NewMons, warps = NewWarps});
do_peer([], State) ->
    State.
